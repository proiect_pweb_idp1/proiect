import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable, of, ReplaySubject } from "rxjs";
import { TranslationDTO } from "../_models/translationDTO";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class TranslationsService {

  baseUrl = environment.apiUrlWithoutToken;

  private translationsSource = new ReplaySubject<TranslationDTO[]>(1);

  translationsSource$ = this.translationsSource.asObservable();

  private translationDictSource = new ReplaySubject<{[id: string]: string}>(1);

  translationDictSource$ = this.translationDictSource.asObservable();

  constructor(private http: HttpClient) {
  }

  getTranslations(languageId: string) {
    let params = new HttpParams();
    params = params.append("languageId", languageId);

    return this.http.get<TranslationDTO[]>(this.baseUrl + 'translations', { observe: "response", params })
      .pipe(map((response) => {
        console.log("Translation services translationSource emitted!");
        this.translationsSource.next(response.body);
        this.createTranslationDictionary(response.body);
      }));
  }

  createTranslationDictionary(response: TranslationDTO[]) {
    let dict: {[id: string]: string} = {};

    for (let translation of response) {
      dict[translation.name] = translation.translatedName;
    }

    this.translationDictSource.next(dict);
  }
}
