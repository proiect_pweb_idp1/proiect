import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { HubConnection, HubConnectionBuilder } from "@microsoft/signalr";
import { ToastrService } from "ngx-toastr";
import { User } from "../_models/user";
import { BehaviorSubject } from "rxjs";
import { take } from "rxjs/operators";
import { Router } from "@angular/router";
import { MemberDTO } from "../_models/memberDTO";

@Injectable({
  providedIn: 'root'
})
export class PresenceService {
  hubUrl = environment.hubUrl;

  private hubConnection: HubConnection;
  private onlineUsersSource = new BehaviorSubject<string[]>([]);
  onlineUsers$ = this.onlineUsersSource.asObservable();

  constructor(private toastr: ToastrService, private router: Router) { }

  createHubConnection(member: MemberDTO) {
    this.hubConnection = new HubConnectionBuilder()
      .withUrl(this.hubUrl + 'presence', {
        accessTokenFactory(): string {
          return member.accessToken;
        }
      })
      .withAutomaticReconnect()
      .build();

    this.hubConnection
      .start()
      .catch(err => console.log(err));

    this.hubConnection.on('UserIsOnline', userId => {
      this.onlineUsers$.pipe(take(1))
        .subscribe(userIds => {
          // updates the online users list with the user that logged in from a new device (and its only device online)
          this.onlineUsersSource.next([...userIds, userId]);
        });
    });

    this.hubConnection.on('UserIsOffline', userId => {
      this.onlineUsers$.pipe(take(1)).subscribe(userIds => {
        // update the online users list by removing the user that logged out from its only remaining connected device
        this.onlineUsersSource.next([...userIds.filter(u => u !== userId)])
      });
    });

    this.hubConnection.on('GetOnlineUsers',
      (userIds: string[]) => {
        this.onlineUsersSource.next(userIds);
    });

    this.hubConnection.on('NewMessageReceived', ({username, knownAs}) => {
      this.toastr.info(username + ' has sent you a new message!')
        .onTap
        .pipe(take(1))
        .subscribe(() => this.router.navigateByUrl('/members/' + username + '?tab=3'));
    });
  }

  stopHubConnection() {
    this.hubConnection
      .stop()
      .catch(err => console.log(err));
  }
}
