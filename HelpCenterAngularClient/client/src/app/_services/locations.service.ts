import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { LocationDTO } from "../_models/locationDTO";
import { environment } from "../../environments/environment";
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LocationsService {

  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
  }

  getLocations(): Observable<LocationDTO[]> {
    return this.http.get<LocationDTO[]>(this.baseUrl + 'locations');
  }
}
