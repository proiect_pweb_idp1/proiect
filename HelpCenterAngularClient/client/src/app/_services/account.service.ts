import { Inject, Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { map, switchMap, take } from "rxjs/operators";
import { Observable, ReplaySubject } from "rxjs";
import { environment } from "../../environments/environment";
import { PresenceService } from "./presence.service";
import { RegisterDTO } from "../_models/registerDTO";
import { MemberDTO } from "../_models/memberDTO";
import { Member } from "../_models/member";
import { AuthService, IdToken } from "@auth0/auth0-angular";
import { DOCUMENT } from "@angular/common";
import { BecomeAVolunteerDTO } from "../_models/becomeAVolunteerDTO";
import { PostsService } from "./posts.service";
import { SubscriptionsService } from "./subscriptions.service";

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  baseUrl = environment.apiUrl;

  private currentMemberSource = new ReplaySubject<MemberDTO>(1);

  currentMember$ = this.currentMemberSource.asObservable();

  constructor(private http: HttpClient, private auth0: AuthService,
              private presence: PresenceService,
              private postsService: PostsService,
              private subscriptionsService: SubscriptionsService,
              @Inject(DOCUMENT) private doc: Document) {
  }

  // login(model: any) {
  //   return this.http.post(this.baseUrl + 'account/login', model).pipe(
  //     map((user: User) => {
  //       if (user) {
  //         this.setCurrentUser(user);
  //         this.presence.createHubConnection(user);
  //       }
  //     })
  //   );
  // }

  protectedAPICall() {
    return this.http.get(this.baseUrl + 'account/protectedAPICall', { responseType: 'text' });
  }

  becomeAVolunteer(becomeAVolunteerDTO: BecomeAVolunteerDTO): Observable<BecomeAVolunteerDTO> {
    return this.http.post<BecomeAVolunteerDTO>(this.baseUrl + 'account/becomeAVolunteer', becomeAVolunteerDTO);
  }

  updateMember(member: Member): Observable<Member> {
    console.log("updateMember in service: ", member);

    return this.http.put<Member>(this.baseUrl + 'account/update', member).pipe(
      map((updatedMember) => {
        if (updatedMember) {
          console.log("account service updatedMember:", updatedMember);
          const memberDTO: MemberDTO = JSON.parse(localStorage.getItem('member'));
          updatedMember.lastActive = new Date();
          localStorage.setItem('member', JSON.stringify({
            ...updatedMember,
            accessToken: memberDTO.accessToken
          } as MemberDTO));
          this.currentMemberSource.next({ ...updatedMember, accessToken: memberDTO.accessToken } as MemberDTO);
          this.postsService.invalidateCache();
          this.subscriptionsService.invalidateCache();
          this.postsService.resetPostParams();
          this.subscriptionsService.resetSubscriptionParams();
          return updatedMember;
        }
        return null;
      })
    );
  }

  getMemberById(memberId: string) {
    return this.http.get<Member>(this.baseUrl + 'account/' + memberId)
      .pipe(map((response) => {
        if (response) {
          return response;
        }
        return null;
      }));
  }

  refreshCurrentMember(memberId: string) {
    this.getMemberById(memberId).pipe(take(1))
      .subscribe((response) => {
        if (response) {
          const memberDTO: MemberDTO = JSON.parse(localStorage.getItem('member'));
          if (memberDTO) {
            console.log("accountService setCurrentMember:", response);
            response.lastActive = new Date();
            localStorage.setItem('member', JSON.stringify({...response, accessToken: memberDTO.accessToken} as MemberDTO));
            this.currentMemberSource.next({...response, accessToken: memberDTO.accessToken} as MemberDTO);
          }
        }
      })
  }

  register() {
    return this.auth0.user$.pipe(switchMap((user) => {
      let registerDTO: RegisterDTO = { id: user.sub, email: user.email };

      return this.http.post(this.baseUrl + 'account/register', registerDTO)
        .pipe(
          map((member: Member) => {
            if (member) {
              return member;
            } else {
              return null;
            }
          })
        );
    }));

    // return this.http.post(this.baseUrl + 'account/register', registerDTO)
    //   .pipe(
    //     map((member: MemberDTO) => {
    //       if (member) {
    //         this.setCurrentMember(member);
    //         this.presence.createHubConnection(member);
    //         return member;
    //       } else {
    //         return null;
    //       }
    //     })
    //   );

    // return this.http.post(this.baseUrl + 'account/register', registerDTO)
    //   .pipe(
    //     map((member: MemberDTO) => {
    //       if (member) {
    //         this.setCurrentMember(member);
    //         this.presence.createHubConnection(member);
    //         return member;
    //       } else {
    //         return null;
    //       }
    //     })
    //   );
  }

  getRoles() {
    return this.auth0.idTokenClaims$.pipe(
      map((token: IdToken) => {
        if (token[environment.rolesUrl]) {
          console.log("accountService get roles:", token[environment.rolesUrl]);
          return token[environment.rolesUrl];
        } else {
          console.log("accountService get roles: null");
          return null;
        }
      })
    );
  }

  getAccessTokenSilently(): Observable<string> {
    return this.auth0.getAccessTokenSilently().pipe(
      map((accessToken: string) => {
        console.log("accountService auth0.getAccesTokenSilently:", accessToken);
        return accessToken;
      })
    );
  }

  setCurrentMember(member: MemberDTO): void {
    // member.roles = [];
    //
    // this.getRoles().subscribe((roles) => {
    //   Array.isArray(roles) ? member.roles = roles : member.roles = [];
    // });
    //
    // let memberAccessToken: string = null;
    //
    // this.getAccessTokenSilently().subscribe((accessToken) => {
    //   memberAccessToken = accessToken;
    // });

    console.log("accountService setCurrentMember:", member);
    member.lastActive = new Date();
    localStorage.setItem('member', JSON.stringify(member));
    this.currentMemberSource.next(member);
    this.presence.createHubConnection(member);
  }

  // setCurrentUser(user: User) {
  //   user.roles = [];
  //   const roleResponseFromToken = this.getDecodedToken(user.token).role;
  //
  //   Array.isArray(roleResponseFromToken) ? user.roles = roleResponseFromToken : user.roles.push(roleResponseFromToken);
  //
  //   localStorage.setItem('user', JSON.stringify(user));
  //   this.currentUserSource.next(user);
  // }

  logout() {
    this.auth0.logout({ client_id: environment.auth.clientId, returnTo: this.doc.location.origin });
    localStorage.removeItem('member');
    this.currentMemberSource.next(null);
    this.presence.stopHubConnection();
  }

  // getDecodedToken(token) {
  //   // atob function decodes the payload of the token
  //   return JSON.parse(atob(token.split(".")[1]));
  // }
}
