import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { LanguageDTO } from "../_models/languageDTO";
import { HttpClient } from "@angular/common/http";
import { Observable, ReplaySubject } from "rxjs";
import { map, take } from "rxjs/operators";
import { TranslationsService } from "./translations.service";

@Injectable({
  providedIn: 'root'
})
export class LanguagesService {

  baseUrl = environment.apiUrlWithoutToken;

  private languagesSource = new ReplaySubject<LanguageDTO[]>(1);

  languagesSource$ = this.languagesSource.asObservable();

  private currentLanguageSource = new ReplaySubject<LanguageDTO>(1);

  currentLanguageSource$ = this.currentLanguageSource.asObservable();

  currentLanguage: LanguageDTO;

  constructor(private http: HttpClient, private translationsServices: TranslationsService) {
  }

  getLanguages(): Observable<LanguageDTO[]> {
    return this.http.get<LanguageDTO[]>(this.baseUrl + 'languages').pipe(
      map((response) => {
        if (response) {
          return response;
        } else {
          return null;
        }
      })
    );
  }

  setLanguages() {
    console.log("languagesService setLanguages");

    this.getLanguages()
      .subscribe((response) => {
        if (response) {
          console.log("emitted languages");
          this.languagesSource.next(response);
          if (!localStorage.getItem('languageId')) {
            console.log("and put languageId for English (default) on localStorage");
            this.setCurrentLanguage(response.find((val) => val.name === 'English')?.id);
          } else {
            this.translationsServices.getTranslations(localStorage.getItem('languageId'))
              .pipe(take(1)).subscribe();
          }
        } else {
          console.log("languagesService.getLanguages returned NULL");
          this.languagesSource.next(null);
        }
      }, (err) => console.log("languagesService error on subscribe:", err));
  }

  setCurrentLanguage(languageId: string) {
    this.languagesSource$.pipe(take(1))
      .subscribe((languages) => {
        if (languages) {
          this.currentLanguage = languages.find((val) => val.id == languageId);
          this.currentLanguageSource.next(this.currentLanguage);
          this.translationsServices.getTranslations(languageId).pipe(take(1)).subscribe();
          localStorage.setItem('languageId', languageId);
          console.log("languagesService setCurrentLanguage id:", languageId);
        }
      });
  }
}
