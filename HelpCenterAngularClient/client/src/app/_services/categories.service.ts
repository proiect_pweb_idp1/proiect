import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { LocationDTO } from "../_models/locationDTO";
import { CategoryDTO } from "../_models/categoryDTO";

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
  }

  getCategories(): Observable<CategoryDTO[]> {
    return this.http.get<CategoryDTO[]>(this.baseUrl + 'categories');
  }
}
