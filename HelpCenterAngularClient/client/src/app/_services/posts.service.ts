import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { map } from "rxjs/operators";
import { getPaginatedResult, getPaginationHeader } from "./paginationHelper";
import { PostDTO } from "../_models/postDTO";
import { PostParams } from "../_models/postParams";
import { AddPostDTO } from "../_models/addPostDTO";
import { PaginatedResult } from "../_models/pagination";

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  baseUrl = environment.apiUrl;

  postsCache = new Map<string, PaginatedResult<PostDTO[]>>();

  postParams: PostParams;

  constructor(private http: HttpClient) {
    this.postParams = new PostParams();
  }

  getPostParams() {
    return this.postParams;
  }

  setPostParams(params: PostParams) {
    this.postParams = params;
  }

  resetPostParams() {
    this.postParams = new PostParams();

    return this.postParams;
  }

  getPosts(postParams: PostParams) {
    let response = this.postsCache.get(Object.values(postParams).join('-'));

    if (response) {
      console.log("GOT FROM CACHE BACK GETPOSTS", response);
      return of(response);
    }

    let params = getPaginationHeader(postParams.pageNumber, postParams.pageSize);

    params = params.append('locationId', postParams.locationId);
    params = params.append('categoryId', postParams.categoryId);
    params = params.append('username', postParams.username);
    params = params.append('orderBy', postParams.orderBy);

    return getPaginatedResult<PostDTO[]>(this.baseUrl + 'posts', params, this.http)
      .pipe(
        map(response => {
          console.log("postsService getPosts response from API:", response);
          this.postsCache.set(Object.values(postParams).join('-'), response);
          return response;
        })
      );
  }

  addPost(addPostDTO: AddPostDTO): Observable<PostDTO> {
    return this.http.post<PostDTO>(this.baseUrl + 'posts', addPostDTO)
      .pipe(map((response) => {
        if (response) {
          this.postsCache = new Map(); // invalidam cache-ul
          return response;
        }
      }));
  }

  updatePost(postDTO: PostDTO) {
    return this.http.put<PostDTO>(this.baseUrl + 'posts/' + postDTO.id, postDTO)
      .pipe(map((response) => {
        if (response) {
          // we need to modify the cache; check every value, iterate over them
          // and modify the posts with the same id
          let newCache = new Map();

          this.postsCache.forEach((value: PaginatedResult<PostDTO[]>, key: string) => {
            let newPaginatedResult = new PaginatedResult<PostDTO[]>();
            newPaginatedResult.pagination = value.pagination;
            newPaginatedResult.result = value.result?.map((post: PostDTO) => {
              if (post.id === postDTO.id) {
                return postDTO;
              } else {
                return post;
              }
            });
            newCache.set(key, newPaginatedResult);
          });

          this.postsCache = newCache;
          return response;
        } else {
          return null;
        }
      }));
  }

  deletePost(postId: string) {
    return this.http.delete(this.baseUrl + 'posts/' + postId, {responseType: 'text'})
      .pipe(map((response) => {
        if (response) {
          // invalida cache-ul!
          this.postsCache = new Map();
          return response;
        } else {
          console.log("delete retured null");
          return null;
        }
      }))
  }

  getPost(postId: string) {
    const post = [...this.postsCache.values()].reduce(
      (arr, elem) => arr.concat(elem.result), [],
    ).find(
      (post: PostDTO) => post.id === postId
    );

    if (post) {
      return of(post);
    }

    return this.http.get<PostDTO>(this.baseUrl + 'posts/' + postId);
  }

  invalidateCache() {
    this.postsCache = new Map<string, PaginatedResult<PostDTO[]>>();
  }

  updateCacheWithPhoto(postDTO: PostDTO) {
    let newCache = new Map();

    this.postsCache.forEach((value: PaginatedResult<PostDTO[]>, key: string) => {
      let newPaginatedResult = new PaginatedResult<PostDTO[]>();
      newPaginatedResult.pagination = value.pagination;
      newPaginatedResult.result = value.result?.map((post: PostDTO) => {
        if (post.id === postDTO.id) {
          return postDTO;
        } else {
          return post;
        }
      });
      newCache.set(key, newPaginatedResult);
    });

    this.postsCache = newCache;
  }

  deletePhoto(photoId: string): Observable<PostDTO> {
    return this.http.delete<PostDTO>(this.baseUrl + 'posts/delete-photo/' + photoId).pipe(
      map((response) => {
        if (response) {
          let newCache = new Map();

          this.postsCache.forEach((value: PaginatedResult<PostDTO[]>, key: string) => {
            let newPaginatedResult = new PaginatedResult<PostDTO[]>();
            newPaginatedResult.pagination = value.pagination;
            newPaginatedResult.result = value.result?.map((post: PostDTO) => {
              if (post.id === response.id) {
                return response;
              } else {
                return post;
              }
            });
            newCache.set(key, newPaginatedResult);
          });

          this.postsCache = newCache;
          return response;
        } else {
          return null;
        }
      })
    )
  }

  // updateMember(post: PostDTO) {
  //   return this.http.put(this.baseUrl + 'posts', post)
  //     .pipe(
  //       map(() => {
  //         const index = this.posts.indexOf(post);
  //         this.posts[index] = post;
  //       })
  //     );
  // }

  // setMainPhoto(photoId: Number) {
  //   return this.http.put(this.baseUrl + 'users/set-main-photo/' + photoId, {});
  // }

  // deletePhoto(photoId: Number) {
  //   return this.http.delete(this.baseUrl + 'users/delete-photo/' + photoId);
  // }

  // addLike(username: string) {
  //   return this.http.post(this.baseUrl + 'likes/' + username, {});
  // }

  // getLikes(predicate: string, pageNumber, pageSize) {
  //   let params = getPaginationHeader(pageNumber, pageSize);
  //
  //   params = params.append('predicate', predicate);
  //
  //   return getPaginatedResult<Partial<Member[]>>(this.baseUrl + 'likes', params, this.http);
  // }

}
