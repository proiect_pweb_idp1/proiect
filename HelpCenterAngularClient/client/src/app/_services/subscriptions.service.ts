import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { map, switchMap, take } from "rxjs/operators";
import { getPaginatedResult, getPaginationHeader } from "./paginationHelper";
import { PaginatedResult } from "../_models/pagination";
import { SubscriptionDTO } from "../_models/subscriptionDTO";
import { SubscriptionParams } from "../_models/subscriptionParams";
import { AccountService } from "./account.service";
import { AddSubscriptionDTO } from "../_models/addSubscriptionDTO";
import { DeleteSubscriptionDTO } from "../_models/deleteSubscriptionDTO";

@Injectable({
  providedIn: 'root'
})
export class SubscriptionsService {

  baseUrl = environment.apiUrl;

  subscriptionsCache = new Map<string, PaginatedResult<SubscriptionDTO[]>>();

  subscriptionParams: SubscriptionParams;

  constructor(private http: HttpClient, private accountService: AccountService) {
    this.initializeSubscriptionParams().pipe(take(1))
      .subscribe((response) => {
        if (response) {
          this.subscriptionParams = response;
          console.log("constructor subscriptions:", this.subscriptionParams);
        }
      });
  }

  initializeSubscriptionParams() {
    return this.accountService.currentMember$.pipe(switchMap((response) => {
      if (response) {
        console.log("memberDTO from initializaSubscriptionParams inside map:", response);
        return of(new SubscriptionParams(response.username));
      }
    }));
  }

  getSubscriptionsParams() {
    return this.subscriptionParams;
  }

  setSubscriptionsParams(params: SubscriptionParams) {
    this.subscriptionParams = params;
  }

  resetSubscriptionParams() {
    return this.initializeSubscriptionParams().pipe(map((response) => {
      if (response) {
        this.subscriptionParams = response;
        console.log("constructor subscriptions:", this.subscriptionParams);
        return this.subscriptionParams;
      }
    }))
  }

  getSubscriptions(subscriptionParams: SubscriptionParams) {
    let response = this.subscriptionsCache.get(Object.values(subscriptionParams).join('-'));

    if (response) {
      console.log("GOT FROM CACHE BACK GETSUBSCRIPTIONS", response);
      return of(response);
    }

    let params = getPaginationHeader(subscriptionParams.pageNumber, subscriptionParams.pageSize);

    params = params.append('locationId', subscriptionParams.locationId);
    params = params.append('categoryId', subscriptionParams.categoryId);
    params = params.append('byUsername', subscriptionParams.byUsername);
    params = params.append('forUsername', subscriptionParams.forUsername);
    params = params.append('orderBy', subscriptionParams.orderBy);

    return getPaginatedResult<SubscriptionDTO[]>(this.baseUrl + 'posts', params, this.http)
      .pipe(
        map(response => {
          console.log("subscriptionService getSubscription response from API:", response);
          this.subscriptionsCache.set(Object.values(subscriptionParams).join('-'), response);
          return response;
        })
      );
  }

  addSubscription(addSubscriptionDTO: AddSubscriptionDTO): Observable<SubscriptionDTO> {
    return this.http.post<SubscriptionDTO>(this.baseUrl + 'subscriptions', addSubscriptionDTO)
      .pipe(map((response) => {
        if (response) {
          this.subscriptionsCache = new Map(); // invalidam cache-ul
          return response;
        } else {
          console.log("add subscription returned null");
          return null;
        }
      }));
  }

  deleteSubscription(deleteSubscriptionDTO: DeleteSubscriptionDTO) {
    return this.http.delete(this.baseUrl + 'subscriptions', { responseType: 'text', body: deleteSubscriptionDTO })
      .pipe(map((response) => {
        if (response) {
          // invalidam cache-ul!
          this.subscriptionsCache = new Map();
          return response;
        } else {
          console.log("delete subscription retured null");
          return null;
        }
      }))
  }

  getSubscription(subscriptionId: string) {
    const subscription = [...this.subscriptionsCache.values()].reduce(
      (arr, elem) => arr.concat(elem.result), [] as SubscriptionDTO[],
    ).find(
      (post) => post.id === subscriptionId
    );

    if (subscription) {
      return of(subscription);
    }

    return this.http.get<SubscriptionDTO>(this.baseUrl + 'subscriptions/' + subscriptionId);
  }

  invalidateCache() {
    this.subscriptionsCache = new Map<string, PaginatedResult<SubscriptionDTO[]>>();
  }

}
