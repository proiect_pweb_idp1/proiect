import { Component, OnInit } from '@angular/core';
import { Member } from "../../_models/member";
import { Pagination } from "../../_models/pagination";
import { PostsService } from "../../_services/posts.service";
import { PostParams } from "../../_models/postParams";
import { CategoryDTO } from "../../_models/categoryDTO";
import { LocationDTO } from "../../_models/locationDTO";
import { LocationsService } from "../../_services/locations.service";
import { CategoriesService } from "../../_services/categories.service";
import { forkJoin } from "rxjs";
import { PostDTO } from "../../_models/postDTO";
import { MemberDTO } from "../../_models/memberDTO";
import { AccountService } from "../../_services/account.service";
import { take } from "rxjs/operators";
import { SubscriptionsService } from "../../_services/subscriptions.service";
import { ToastrService } from "ngx-toastr";
import { AddSubscriptionDTO } from "../../_models/addSubscriptionDTO";

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.css']
})
export class PostsListComponent implements OnInit {

  posts: PostDTO[] = [];
  pagination: Pagination;
  postParams: PostParams;
  locations: LocationDTO[];
  categories: CategoryDTO[];

  currentMember: MemberDTO;

  constructor(private postsService: PostsService,
              private subscriptionsService: SubscriptionsService,
              private accountService: AccountService,
              private toastrService: ToastrService,
              private locationsService: LocationsService,
              private categoriesService: CategoriesService) {
    this.postParams = this.postsService.getPostParams();
  }

  ngOnInit(): void {
    this.initializeAll();
  }

  initializeAll() {
    forkJoin({
      locationsRequest: this.locationsService.getLocations(),
      categoriesRequest: this.categoriesService.getCategories(),
      currMember: this.accountService.currentMember$.pipe(take(1)),
    }).subscribe(({ locationsRequest, categoriesRequest, currMember }) => {
      this.locations = locationsRequest;
      this.categories = categoriesRequest;
      this.currentMember = currMember;
      this.loadPosts();
    });
  }

  loadPosts() {
    this.postsService.setPostParams(this.postParams);
    console.log("loadPosts postParams:", this.postParams);

    this.postsService.getPosts(this.postParams).subscribe(
      response => {
        this.posts = response.result;
        this.pagination = response.pagination;
      }
    );
  }

  addSubscriptionForCategory(forCategoryId: string) {
    let addSubDTO: AddSubscriptionDTO = { forCategoryId: forCategoryId, byUserId: this.currentMember.id, forUserId: "", forLocationId: "" };
    this.subscriptionsService.addSubscription(addSubDTO).pipe(take(1))
      .subscribe((response) => {
        if (response) {
          // TO DO: updatezi currentMember source cu vectorul de subscriptionsForIds
          // ca sa se refaca UI-ul IN INTERIORUL ACCOUNTSERVICE CA SA EMITI DIRECT DIN NOU CURRENTMEMBER !!
          this.accountService.refreshCurrentMember(this.currentMember.id);
          this.toastrService.success("Successfully subscribed to " + response.forCategoryName);
        } else {
          console.log("add subscription for category returned NULL");
        }
      }, (err) => console.log(err));
  }

  addSubscriptionForLocation(forLocationId: string) {
    let addSubDTO: AddSubscriptionDTO = { forLocationId: forLocationId, byUserId: this.currentMember.id, forUserId: "", forCategoryId: "" };
    this.subscriptionsService.addSubscription(addSubDTO).pipe(take(1))
      .subscribe((response) => {
        if (response) {
          // TO DO: updatezi currentMember source cu vectorul de subscriptionsForIds
          // ca sa se refaca UI-ul IN INTERIORUL ACCOUNTSERVICE CA SA EMITI DIRECT DIN NOU CURRENTMEMBER !!
          this.accountService.refreshCurrentMember(this.currentMember.id);
          this.toastrService.success("Successfully subscribed to " + response.forLocationName);
        } else {
          console.log("add subscription for location returned NULL");
        }
      }, (err) => console.log(err));
  }

  resetFilters() {
    this.postParams = this.postsService.resetPostParams();
    this.loadPosts();
  }

  pageChanged(event: any) {
    if (this.postParams.pageNumber !== event.page) {
      this.postParams.pageNumber = event.page;
      this.postsService.setPostParams(this.postParams);
      this.loadPosts();
    }
  }

}
