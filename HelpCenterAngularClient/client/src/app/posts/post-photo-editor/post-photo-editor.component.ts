import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FileUploader } from "ng2-file-upload";
import { environment } from "../../../environments/environment";
import { MemberDTO } from "../../_models/memberDTO";
import { AccountService } from "../../_services/account.service";
import { take } from "rxjs/operators";
import { PostDTO } from "../../_models/postDTO";
import { PostsService } from "../../_services/posts.service";
import { Photo } from "../../_models/photo";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";

@Component({
  selector: 'app-post-photo-editor',
  templateUrl: './post-photo-editor.component.html',
  styleUrls: ['./post-photo-editor.component.css']
})
export class PostPhotoEditorComponent implements OnInit {

  @Input() post: PostDTO;
  @Output() emitDisabledByPhotoDelete = new EventEmitter<boolean>();

  uploader: FileUploader;
  hasBaseDropzoneOver = false;
  baseUrl = environment.apiUrl;

  //user: User;
  currentMember: MemberDTO;
  isDisabled = false;

  constructor(private accountService: AccountService,
              private postsService: PostsService,
              private router: Router,
              private toastrService: ToastrService) {
    this.accountService.currentMember$.pipe(take(1))
      .subscribe(m => this.currentMember = m);
  }

  ngOnInit(): void {
    this.initializeUploader();
  }

  fileOverBase(e: any) {
    this.hasBaseDropzoneOver = e;
  }

  emitDeleteDisabledEvent() {
    this.emitDisabledByPhotoDelete.emit(true);
  }

  deletePhoto(photoId: string) {
    this.postsService.deletePhoto(photoId).pipe(take(1))
      .subscribe((response) => {
        if (response) {
          this.post.photos = this.post.photos.filter(p => p.id !== photoId);
          this.emitDeleteDisabledEvent();
          this.isDisabled = true;
          this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
            this.router.navigateByUrl('/posts/edit/' + this.post.id).then(() => {
              this.toastrService.success("Photo deleted successfully!");
            });
          });
        } else {
          this.toastrService.error("Photo could not be deleted!");
          console.log("delete photo response NULL");
        }
      }, (err) => {
        console.log("delete photo error:", err);
        this.toastrService.error("Photo could not be deleted!");
      });
  }

  initializeUploader() {
    this.uploader = new FileUploader({
      url: this.baseUrl + 'posts/add-photo/' + this.post.id,
      authToken: 'Bearer ' + this.currentMember.accessToken,
      isHTML5: true,
      allowedFileType: ['image'],
      removeAfterUpload: true,
      autoUpload: false,
      maxFileSize: 10 * 1024 * 1024,
    });

    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    }

    this.uploader.onAfterAddingAll((file) => {
      file.withCredentials = false;
    });

    this.uploader.onCompleteAll = () => {
      console.log("photo response COMPLETE ALL");
      location.reload();
    }

    this.uploader.onSuccessItem = (item, response, status, headers) => {
      if (response) {
        console.log("on success item:", response);
        const photo: Photo = JSON.parse(response);

        this.post.photos.push(photo);

        this.postsService.updateCacheWithPhoto(this.post);
      }
    }

    this.uploader.onErrorItem = (item, response, status, headers) => {
      if (response) {
        console.log("error on add photo:", response);
        this.toastrService.error("Error uploading photo!");
      }
    }
  }
}
