import { Component, HostListener, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MemberDTO } from "../../_models/memberDTO";
import { AccountService } from "../../_services/account.service";
import { TranslationsService } from "../../_services/translations.service";
import { ToastrService } from "ngx-toastr";
import { take } from "rxjs/operators";
import { ValidateNumber } from "../../_validators/number.validator";
import { Member } from "../../_models/member";
import { LocationDTO } from "../../_models/locationDTO";
import { CategoryDTO } from "../../_models/categoryDTO";
import { LocationsService } from "../../_services/locations.service";
import { CategoriesService } from "../../_services/categories.service";
import { forkJoin } from "rxjs";
import { AddPostDTO } from "../../_models/addPostDTO";
import { PostsService } from "../../_services/posts.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-post-add',
  templateUrl: './post-add.component.html',
  styleUrls: ['./post-add.component.css']
})
export class PostAddComponent implements OnInit {

  myForm: FormGroup;

  @HostListener('window:beforeunload', ['$event']) unloadNotification($event: any) {
    if (this.myForm.dirty) {
      $event.returnValue = true;
    }
  }

  validationErrors: string[] = [];

  currentMember: MemberDTO;
  isDisabled = false;

  locations: LocationDTO[];
  categories: CategoryDTO[];

  constructor(public accountService: AccountService,
              private locationsService: LocationsService,
              private postsService: PostsService,
              private router: Router,
              private categoriesService: CategoriesService,
              public translationsService: TranslationsService,
              private toastrService: ToastrService,
              private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.startUpAll();
  }

  startUpAll() {
    this.setMember();
    this.getTranslations();
    this.initializeForm();
  }

  setMember() {
    this.accountService.currentMember$.pipe(take(1))
      .subscribe((member: MemberDTO) => {
        this.currentMember = { ...member };
      });
  }

  getTranslations() {
    this.translationsService.translationsSource$.pipe(take(1))
      .subscribe();
  }

  initializeForm() {
    forkJoin({
      locationsRequest: this.locationsService.getLocations(),
      categoriesRequest: this.categoriesService.getCategories()
    }).subscribe(({ locationsRequest, categoriesRequest }) => {
      this.locations = locationsRequest;
      this.categories = categoriesRequest;
      this.myForm = this.fb.group({
        title: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(25)]],
        description: ['', [Validators.required, Validators.minLength(10), Validators.max(250)]],
        postCategoryId: ['', Validators.required],
        postLocationId: ['', Validators.required],
      });
    });
  }

  addPost() {
    this.postsService.addPost({ ...this.myForm.value, postUserId: this.currentMember.id } as AddPostDTO)
      .pipe(take(1))
      .subscribe((response) => {
        if (response) {
          this.isDisabled = true;
          console.log("Post added successfully,", response);
          this.toastrService.success("Post sucessfully added!").onHidden.subscribe(() => {
            this.router.navigateByUrl('/posts/edit/' + response.id);
          });
        } else {
          this.toastrService.error("Could not add the post!");
          this.startUpAll();
        }
      });
  }

  // updateMember() {
  //   this.accountService.updateMember({
  //     ...this.myForm.value,
  //     id: this.currentMember.id,
  //     email: this.currentMember.email,
  //     lastActive: this.currentMember.lastActive,
  //     dateCreated: this.currentMember.dateCreated
  //   } as Member)
  //     .subscribe((response: Member) => {
  //       if (response) {
  //         this.toastrService.success('Profile updated successfully!');
  //         this.setMember();
  //       } else {
  //         console.log("response from Profile UpdateMember is null, error received?");
  //       }
  //     }, (error) => this.validationErrors = error);
  // }

}
