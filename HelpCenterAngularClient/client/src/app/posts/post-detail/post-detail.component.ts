import { Component, OnInit } from '@angular/core';
import { AddSubscriptionDTO } from "../../_models/addSubscriptionDTO";
import { take } from "rxjs/operators";
import { PostsService } from "../../_services/posts.service";
import { AccountService } from "../../_services/account.service";
import { SubscriptionsService } from "../../_services/subscriptions.service";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { MemberDTO } from "../../_models/memberDTO";
import { forkJoin } from "rxjs";
import { PostDTO } from "../../_models/postDTO";
import { TranslationsService } from "../../_services/translations.service";

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css']
})
export class PostDetailComponent implements OnInit {

  currentMember: MemberDTO;

  postId: string;
  currentPost: PostDTO;

  constructor(private postsService: PostsService,
              public accountService: AccountService,
              private route: ActivatedRoute,
              private subscriptionsService: SubscriptionsService,
              public translationsService: TranslationsService,
              private router: Router,
              private toastrService: ToastrService) {
  }

  ngOnInit(): void {
    forkJoin(({
      paramMap: this.route.paramMap,
      currMember: this.accountService.currentMember$.pipe(take(1)),
      translations: this.translationsService.translationsSource$.pipe(take(1))
    })).subscribe(({paramMap, currMember, translations}) => {
      this.currentMember = currMember;
      paramMap.get('id') ? this.postId =  paramMap.get('id') : this.postId = null;
    })
  }

  addSubscriptionForCategory(forCategoryId: string) {
    let addSubDTO: AddSubscriptionDTO = { forCategoryId: forCategoryId, byUserId: this.currentMember.id, forUserId: "", forLocationId: "" };
    this.subscriptionsService.addSubscription(addSubDTO).pipe(take(1))
      .subscribe((response) => {
        if (response) {
          // TO DO: updatezi currentMember source cu vectorul de subscriptionsForIds
          // ca sa se refaca UI-ul IN INTERIORUL ACCOUNTSERVICE CA SA EMITI DIRECT DIN NOU CURRENTMEMBER !!
          //this.accountService.addSubscriptionIdToUser(response.forCategoryId);
          this.toastrService.success("Successfully subscribed to " + response.forCategoryName);
        } else {
          console.log("add subscription for category returned NULL");
        }
      }, (err) => console.log(err));
  }

  addSubscriptionForLocation(forLocationId: string) {
    let addSubDTO: AddSubscriptionDTO = { forLocationId: forLocationId, byUserId: this.currentMember.id, forUserId: "", forCategoryId: "" };
    this.subscriptionsService.addSubscription(addSubDTO).pipe(take(1))
      .subscribe((response) => {
        if (response) {
          // TO DO: updatezi currentMember source cu vectorul de subscriptionsForIds
          // ca sa se refaca UI-ul IN INTERIORUL ACCOUNTSERVICE CA SA EMITI DIRECT DIN NOU CURRENTMEMBER !!
          //this.accountService.addSubscriptionIdToUser(response.forCategoryId);
          this.toastrService.success("Successfully subscribed to " + response.forLocationName);
        } else {
          console.log("add subscription for location returned NULL");
        }
      }, (err) => console.log(err));
  }

  addSubscriptionForMember() {
    let addSubDTO: AddSubscriptionDTO = { forLocationId: "", byUserId: this.currentMember.id, forUserId: this.currentPost.postUserId, forCategoryId: "" };
    this.subscriptionsService.addSubscription(addSubDTO).pipe(take(1))
      .subscribe((response) => {
        if (response) {
          this.accountService.refreshCurrentMember(this.currentMember.id);
          this.toastrService.success("Successfully subscribed to " + response.forUsername + "'posts");
        } else {
          console.log("add subscription for location returned NULL");
        }
      }, (err) => console.log(err));
  }

}
