import { Component, Input, OnInit } from '@angular/core';
import { PostDTO } from "../../_models/postDTO";
import { PostsService } from "../../_services/posts.service";
import { AccountService } from "../../_services/account.service";
import { MemberDTO } from "../../_models/memberDTO";
import { take } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
import { AddSubscriptionDTO } from "../../_models/addSubscriptionDTO";
import { SubscriptionsService } from "../../_services/subscriptions.service";

@Component({
  selector: 'app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.css']
})
export class PostCardComponent implements OnInit {

  @Input() post: PostDTO;

  currentMember: MemberDTO;
  isDisabled = false;

  constructor(private postsService: PostsService,
              private accountService: AccountService,
              private subscriptionsService: SubscriptionsService,
              private router: Router,
              private toastrService: ToastrService) {
  }

  ngOnInit(): void {
    this.accountService.currentMember$.pipe(take(1))
      .subscribe((response) => {
        this.currentMember = response;
      });
  }

  addSubscriptionForMember(forLocationId: string) {
    let addSubDTO: AddSubscriptionDTO = { forLocationId: "", byUserId: this.currentMember.id, forUserId: this.post.postUserId, forCategoryId: "" };
    this.subscriptionsService.addSubscription(addSubDTO).pipe(take(1))
      .subscribe((response) => {
        if (response) {
          // TO DO: updatezi currentMember source cu vectorul de subscriptionsForIds (CONSTRUIT IN API LA GET USER !!!! )
          // ca sa se refaca UI-ul IN INTERIORUL ACCOUNTSERVICE CA SA EMITI DIRECT DIN NOU CURRENTMEMBER !!
          //this.accountService.emitNewCurrentMember(response.forCategoryId); DAR IN INTERIORUL SERVICIULUI DE ACCOUNT
          this.toastrService.success("Successfully subscribed to " + response.forUsername + "'posts");
        } else {
          console.log("add subscription for location returned NULL");
        }
      }, (err) => console.log(err));
  }

  deletePost(postId: string) {
    return this.postsService.deletePost(postId).pipe(take(1))
      .subscribe((response) => {
        if (response) {
          this.isDisabled = true;
          this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
            this.router.navigateByUrl('/posts').then(() => {
              this.toastrService.success("Post deleted successfully!");
            });
          });
        } else {
          this.toastrService.error("Error deleting post!");
        }
      }, (err) => {
        console.log("delete Post err:", err);
        this.toastrService.error("Error deleting post!");
      });
  }

}
