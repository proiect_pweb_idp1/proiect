import { Component, HostListener, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MemberDTO } from "../../_models/memberDTO";
import { LocationDTO } from "../../_models/locationDTO";
import { CategoryDTO } from "../../_models/categoryDTO";
import { AccountService } from "../../_services/account.service";
import { LocationsService } from "../../_services/locations.service";
import { PostsService } from "../../_services/posts.service";
import { ActivatedRoute, Router } from "@angular/router";
import { CategoriesService } from "../../_services/categories.service";
import { TranslationsService } from "../../_services/translations.service";
import { ToastrService } from "ngx-toastr";
import { take } from "rxjs/operators";
import { forkJoin } from "rxjs";
import { PostDTO } from "../../_models/postDTO";
import { NgxGalleryAnimation, NgxGalleryImage, NgxGalleryOptions } from "@kolkov/ngx-gallery";

@Component({
  selector: 'app-post-edit',
  templateUrl: './post-edit.component.html',
  styleUrls: ['./post-edit.component.css']
})
export class PostEditComponent implements OnInit {

  myForm: FormGroup;

  @HostListener('window:beforeunload', ['$event']) unloadNotification($event: any) {
    if (this.myForm.dirty) {
      $event.returnValue = true;
    }
  }

  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];

  validationErrors: string[] = [];

  currentMember: MemberDTO;
  currentPost: PostDTO;
  postId: string;
  isDisabledByChild = false;

  locations: LocationDTO[];
  categories: CategoryDTO[];

  constructor(public accountService: AccountService,
              private locationsService: LocationsService,
              private postsService: PostsService,
              private router: Router,
              private route: ActivatedRoute,
              private categoriesService: CategoriesService,
              public translationsService: TranslationsService,
              private toastrService: ToastrService,
              private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.startUpAll();
  }

  startUpAll() {
    this.route.paramMap.subscribe(params => {
      console.log("query params = ", params);
      params.get('id') ? this.postId = params.get('id') : this.postId = null;
      this.setMember();
      this.getTranslations();
      this.initializeForm();
    });
  }

  getImages(): NgxGalleryImage[] {
    const imageUrls = [];

    for (const photo of this.currentPost.photos) {
      imageUrls.push(
        {
          small: photo?.url,
          medium: photo?.url,
          big: photo?.url,
        }
      );
    }

    return imageUrls;
  }

  setMember() {
    this.accountService.currentMember$.pipe(take(1))
      .subscribe((member: MemberDTO) => {
        this.currentMember = { ...member };
      });
  }

  getTranslations() {
    this.translationsService.translationsSource$.pipe(take(1))
      .subscribe();
  }

  isDisabledByPhotoDelete(disabledByChild: boolean) {
    this.isDisabledByChild = disabledByChild;
  }

  initializeForm() {
    forkJoin({
      locationsRequest: this.locationsService.getLocations(),
      categoriesRequest: this.categoriesService.getCategories(),
      currPost: this.postsService.getPost(this.postId),
    }).subscribe(({ locationsRequest, categoriesRequest, currPost }) => {
      this.locations = locationsRequest;
      this.categories = categoriesRequest;
      if (currPost) {
        this.currentPost = currPost;
        this.galleryOptions = [
          {
            width: '300px',
            height: '300px',
            imagePercent: 100,
            thumbnailsColumns: 4,
            imageAnimation: NgxGalleryAnimation.Slide,
            preview: false
          },
        ];

        this.galleryImages = this.getImages();
        this.myForm = this.fb.group({
          title: [this.currentPost.title, [Validators.required, Validators.minLength(6), Validators.maxLength(25)]],
          description: [this.currentPost.description, [Validators.required, Validators.minLength(10), Validators.max(250)]],
          postCategoryId: [this.currentPost.postCategoryId, Validators.required],
          postLocationId: [this.currentPost.postLocationId, Validators.required],
        });
      }
    });
  }

  updatePost() {
    this.postsService.updatePost({
      ...this.currentPost,
      title: this.myForm.controls.title.value,
      description: this.myForm.controls.description.value,
      postCategoryId: this.myForm.controls.postCategoryId.value,
      postLocationId: this.myForm.controls.postLocationId.value,
    } as PostDTO).pipe(take(1))
      .subscribe((response: PostDTO) => {
        if (response) {
          console.log("response from Profile UpdateMember is:", response);
          this.toastrService.success('Profile updated successfully!');
          this.setMember();
          this.initializeForm();
        } else {
          console.log("response from Profile UpdateMember is null, error received?");
        }
      }, (error) => console.log("UPDATE POST error:", error));
  }

}
