import { Component, Input, OnInit, Self } from '@angular/core';
import { ControlValueAccessor, NgControl } from "@angular/forms";

@Component({
  selector: 'app-text-area-input',
  templateUrl: './text-area-input.component.html',
  styleUrls: ['./text-area-input.component.css']
})
export class TextAreaInputComponent implements ControlValueAccessor {
  @Input() label: string;
  @Input() rows: string;

  constructor(@Self() public ngControl: NgControl) {
    this.ngControl.valueAccessor = this;
  }

  registerOnChange(fn: any): void {

  }

  registerOnTouched(fn: any): void {

  }

  writeValue(obj: any): void {

  }
}
