import { Component, Input, OnInit } from '@angular/core';
import { PostDTO } from "../../_models/postDTO";
import { MemberDTO } from "../../_models/memberDTO";
import { PostsService } from "../../_services/posts.service";
import { AccountService } from "../../_services/account.service";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { take } from "rxjs/operators";
import { SubscriptionsService } from "../../_services/subscriptions.service";
import { DeleteSubscriptionDTO } from "../../_models/deleteSubscriptionDTO";
import { SubscriptionDTO } from "../../_models/subscriptionDTO";

@Component({
  selector: 'app-subscription-card',
  templateUrl: './subscription-card.component.html',
  styleUrls: ['./subscription-card.component.css']
})
export class SubscriptionCardComponent implements OnInit {

  @Input() subscription: SubscriptionDTO;

  currentMember: MemberDTO;
  isDisabled = false;

  constructor(private subscriptionsService: SubscriptionsService,
              private accountService: AccountService,
              private router: Router,
              private toastrService: ToastrService) {
  }

  ngOnInit(): void {
    console.log("SUBSCRIPTION Card INPUT:", this.subscription);

    this.accountService.currentMember$.pipe(take(1))
      .subscribe((response) => {
        this.currentMember = response;
      });
  }

  deleteSubscription(subscriptionId: string) {
    let deleteSubDTO: DeleteSubscriptionDTO =
      {
        forLocationId: this.subscription.forLocationId,
        forCategoryId: this.subscription.forCategoryId,
        forUserId: this.subscription.forUserId,
        byUserId: this.subscription.byUserId,
      } as DeleteSubscriptionDTO;

    // TO DO: ADD SI DELETE SUBSCRIBE DIN POSTS-LIST DROPDOWNS SI DIN PROFILE DETAILS AL ALTUI USER !!!!

    return this.subscriptionsService.deleteSubscription(deleteSubDTO).pipe(take(1))
      .subscribe((response) => {
        if (response) {
          this.isDisabled = true;
          this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
            this.router.navigateByUrl('/subscriptions').then(() => {
              this.toastrService.success("Subscription deleted successfully!");
            });
          });
        } else {
          this.toastrService.error("Error deleting subscription!");
        }
      }, (err) => {
        console.log("delete Post err:", err);
        this.toastrService.error("Error deleting subscription!");
      });
  }

}
