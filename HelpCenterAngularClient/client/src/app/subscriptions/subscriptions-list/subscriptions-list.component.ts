import { Component, OnInit } from '@angular/core';
import { PostDTO } from "../../_models/postDTO";
import { Pagination } from "../../_models/pagination";
import { PostParams } from "../../_models/postParams";
import { LocationDTO } from "../../_models/locationDTO";
import { CategoryDTO } from "../../_models/categoryDTO";
import { MemberDTO } from "../../_models/memberDTO";
import { PostsService } from "../../_services/posts.service";
import { AccountService } from "../../_services/account.service";
import { LocationsService } from "../../_services/locations.service";
import { CategoriesService } from "../../_services/categories.service";
import { forkJoin } from "rxjs";
import { take } from "rxjs/operators";
import { SubscriptionDTO } from "../../_models/subscriptionDTO";
import { SubscriptionParams } from "../../_models/subscriptionParams";
import { SubscriptionsService } from "../../_services/subscriptions.service";

@Component({
  selector: 'app-subscriptions-list',
  templateUrl: './subscriptions-list.component.html',
  styleUrls: ['./subscriptions-list.component.css']
})
export class SubscriptionsListComponent implements OnInit {

  subscriptions: SubscriptionDTO[] = [];
  pagination: Pagination;
  subscriptionParams: SubscriptionParams;
  locations: LocationDTO[];
  categories: CategoryDTO[];

  currentMember: MemberDTO;

  constructor(private subscriptionService: SubscriptionsService,
              private accountService: AccountService,
              private locationsService: LocationsService,
              private categoriesService: CategoriesService) {
  }

  ngOnInit(): void {
    this.initializeAll();
  }

  initializeAll() {
    forkJoin({
      locationsRequest: this.locationsService.getLocations(),
      categoriesRequest: this.categoriesService.getCategories(),
      currMember: this.accountService.currentMember$.pipe(take(1)),
      subsParams: this.subscriptionService.initializeSubscriptionParams().pipe(take(1))
    }).subscribe(({ locationsRequest, categoriesRequest, currMember, subsParams }) => {
      this.locations = locationsRequest;
      this.categories = categoriesRequest;
      this.currentMember = currMember;
      this.subscriptionParams = subsParams;
      this.loadSubscriptions();
    });
  }

  loadSubscriptions() {
    this.subscriptionService.setSubscriptionsParams(this.subscriptionParams);
    console.log("loadSubscriptions subscriptionParams:", this.subscriptionParams);

    this.subscriptionService.getSubscriptions(this.subscriptionParams).subscribe(
      response => {
        this.subscriptions = response.result;
        this.pagination = response.pagination;
      }
    );
  }

  resetFilters() {
    this.subscriptionService.resetSubscriptionParams().pipe(take(1))
      .subscribe((response) => {
        if (response) {
          this.subscriptionParams = response;
          this.loadSubscriptions();
        }
      });
  }

  pageChanged(event: any) {
    if (this.subscriptionParams.pageNumber !== event.page) {
      this.subscriptionParams.pageNumber = event.page;
      this.loadSubscriptions();
    }
  }

}
