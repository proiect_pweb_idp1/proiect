import { Component, Inject, OnInit } from '@angular/core';
import { AccountService } from "../_services/account.service";
import { Observable } from "rxjs";
import { User } from "../_models/user";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { AuthService } from "@auth0/auth0-angular";
import { DOCUMENT } from '@angular/common';
import { environment } from "../../environments/environment";
import { LanguageDTO } from "../_models/languageDTO";
import { LanguagesService } from "../_services/languages.service";
import { take } from "rxjs/operators";
import { TranslationsService } from "../_services/translations.service";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  //loginConfig = { client_id: environment.auth.clientId, display: "popup", max_age: 600000, ui_locales: "en ro uk", redirectUri: "https://localhost:4200/welcome" };
  loginConfig = {
    appState:
      { target: '/welcome' }
  };
  //registerConfig = { client_id: environment.auth.clientId, display: "popup", max_age: 600000, ui_locales: "en ro uk", screen_hint: "signup", redirectUri: "https://localhost:4200/welcome" };
  registerConfig = {
    screen_hint: "signup",
    appState:
      { target: '/welcome' }
  };

  constructor(public auth0: AuthService, public accountService: AccountService, public translationsService: TranslationsService,
              private router: Router, public languagesService: LanguagesService,
              private toastrService: ToastrService) {
  }

  ngOnInit(): void {
    console.log("navComponent ngOnInit");
    this.languagesService.languagesSource$.pipe(take(1))
      .subscribe((response) => {
        if (response) {
          if (localStorage.getItem('languageId')) {
            this.setCurrentLanguage(localStorage.getItem('languageId'));
          }
        } else {
          console.log("NAV NGONINIT: Something unexpected went wrong. Response from languages Source is null");
        }
      })
  }

  // login() {
  //   this.accountService.login(this.model).subscribe(response => {
  //     this.router.navigateByUrl('/members');
  //   });
  // }

  setCurrentLanguage(languageId: string) {
    this.languagesService.setCurrentLanguage(languageId);
  }

  register(): void {
    this.loginWithRedirect(this.registerConfig);
  }

  login(): void {
    this.loginWithRedirect(this.loginConfig);
  }

  loginWithRedirect(config): void {
    this.auth0.loginWithRedirect(config);
  }

  logout(): void {
    this.accountService.logout();
  }

}
