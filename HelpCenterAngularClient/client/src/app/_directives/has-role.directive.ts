import { Directive, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { AccountService } from "../_services/account.service";
import { take } from "rxjs/operators";
import { Member } from "../_models/member";
import { MemberDTO } from "../_models/memberDTO";

@Directive({
  selector: '[appHasRole]' // this will be *appHasRole='["Admin", ...]'
  // Angular examples used: bsRadio, *ngIf, *ngFor
})
export class HasRoleDirective implements OnInit {
  @Input() appHasRole: string[];
  member: MemberDTO;

  constructor(private viewContainerRef: ViewContainerRef,
              private templateRef: TemplateRef<any>,
              private accountService: AccountService) {
    this.accountService.currentMember$.pipe(take(1)).subscribe(member => {
      this.member = member;
    })
  }

  ngOnInit(): void {
    // clear the view if the user has no roles
    if (!this.member?.roles || this.member == null) {
      this.viewContainerRef.clear();
      return;
    }

    if (this.member?.roles.some(r => this.appHasRole.includes(r))) {
      this.viewContainerRef.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainerRef.clear();
    }
  }

}
