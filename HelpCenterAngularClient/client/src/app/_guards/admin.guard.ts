import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {ToastrService} from "ngx-toastr";
import {AccountService} from "../_services/account.service";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  constructor(private accountService: AccountService, private toastr: ToastrService) {}

  canActivate(): Observable<boolean> {
    return this.accountService.currentMember$.pipe(
      map(member => {
        if (member == null) {
          this.toastr.error("You cannot enter this section!");
          return false;
        }

        if (member.roles.includes("help-center:admin")) {
          return true;
        }

        this.toastr.error("You cannot enter this section!");
      })
    );
  }

}
