import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { ToastrService } from "ngx-toastr";
import { map } from "rxjs/operators";
import { AuthService } from "@auth0/auth0-angular";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private auth0: AuthService, private router: Router, private toastrService: ToastrService) {
  }

  canActivate(): Observable<boolean> {
    return this.auth0.user$.pipe(
      map(user => {
        if (user) {
          return true;
        }
        this.toastrService.error('You must authenticate first!');
        this.router.navigateByUrl("/");
      })
    );
  }

}
