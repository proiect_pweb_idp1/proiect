export interface TranslationDTO {
  id: string;
  languageId: string;
  languageName: string;
  name: string;
  translatedName: string;
}
