import { User } from "./user";
import { Member } from "./member";

export class SubscriptionParams {
  locationId: string;
  categoryId: string;
  byUsername: string;
  forUsername: string;
  orderBy: string;
  pageNumber = 1;
  pageSize = 5;

  constructor(byUsername: string) {
    this.locationId = "";
    this.categoryId = "";
    this.forUsername = "";
    this.byUsername = byUsername;
    this.orderBy = "";
    this.pageNumber = 1;
    this.pageSize = 5;
  }
}
