export interface AddPostDTO {
  title: string;
  description: string;
  postUserId: string;
  postCategoryId: string;
  postLocationId: string;
}
