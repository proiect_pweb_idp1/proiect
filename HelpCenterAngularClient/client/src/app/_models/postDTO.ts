import { Photo } from "./photo";

export interface PostDTO {
  id: string;
  title: string;
  dateCreated: Date;
  description: string;
  postUserId: string;
  postUsername: string;
  postLocationId: string;
  postLocationName: string;
  postCategoryId: string;
  postCategoryName: string;
  photos: Photo[];
}
