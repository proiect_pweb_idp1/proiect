export interface AddSubscriptionDTO {
  byUserId: string;
  forUserId: string;
  forCategoryId: string;
  forLocationId: string;
}
