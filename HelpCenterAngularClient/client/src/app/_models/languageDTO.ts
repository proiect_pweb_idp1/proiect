export interface LanguageDTO {
  id: string;
  name: string;
}
