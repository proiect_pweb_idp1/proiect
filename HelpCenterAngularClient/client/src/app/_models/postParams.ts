import { User } from "./user";
import { Member } from "./member";

export class PostParams {
  locationId: string;
  categoryId: string;
  username: string;
  orderBy: string;
  pageNumber = 1;
  pageSize = 5;

  constructor() {
    this.locationId = "";
    this.categoryId = "";
    this.username = "";
    this.orderBy = "";
    this.pageNumber = 1;
    this.pageSize = 5;
  }
}
