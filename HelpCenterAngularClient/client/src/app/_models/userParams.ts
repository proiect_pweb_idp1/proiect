import { User } from "./user";
import { Member } from "./member";

export class UserParams {
  minAge = 18;
  maxAge = 99;
  pageNumber = 1;
  pageSize = 24;

  constructor() {}
}
