export interface LocationDTO {
  id: string;
  name: string;
  isDefault: boolean;
}
