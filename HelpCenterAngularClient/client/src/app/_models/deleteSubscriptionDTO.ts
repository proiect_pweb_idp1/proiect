export interface DeleteSubscriptionDTO {
  byUserId: string;
  forUserId: string;
  forCategoryId: string;
  forLocationId: string;
}
