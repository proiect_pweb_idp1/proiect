export interface SubscriptionDTO {
  id: string;
  dateCreated: Date;
  byUserId: string;
  byUsername: string;
  forUserId: string;
  forUsername: string;
  forLocationId: string;
  forLocationName: string;
  forCategoryId: string;
  forCategoryName: string;
}
