export interface CategoryDTO {
  id: string;
  name: string;
  isDefault: boolean;
}
