export interface MemberDTO {
  id: string;
  accessToken: string;
  email: string;
  username: string;
  about: string;
  phoneNumber: string;
  dateCreated: Date;
  lastActive: Date;
  age: number;
  roles: string[];
  subscriptionIds: string[];
}
