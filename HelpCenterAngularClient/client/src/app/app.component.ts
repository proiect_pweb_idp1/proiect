import { Component, OnInit } from '@angular/core';
import { AccountService } from "./_services/account.service";
import { PresenceService } from "./_services/presence.service";
import { AuthService } from "@auth0/auth0-angular";
import { take } from "rxjs/operators";
import { MemberDTO } from "./_models/memberDTO";
import { LanguagesService } from "./_services/languages.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Help Center';

  users: any;

  constructor(private accountService: AccountService, private languagesService: LanguagesService) {
  }

  ngOnInit(): void {
    this.setCurrentMember();
    this.setLanguages();
  }

  setCurrentMember() {
    const member: MemberDTO = JSON.parse(localStorage.getItem('member'));
    console.log("appComponent ngOnInit member:", member);

    if (member) {
      this.accountService.setCurrentMember(member);
    }
  }

  setLanguages() {
    this.languagesService.setLanguages();
  }
}
