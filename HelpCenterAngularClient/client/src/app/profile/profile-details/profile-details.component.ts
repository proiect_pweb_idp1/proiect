import { Component, OnInit } from '@angular/core';
import { AddSubscriptionDTO } from "../../_models/addSubscriptionDTO";
import { take } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";
import { SubscriptionsService } from "../../_services/subscriptions.service";
import { AccountService } from "../../_services/account.service";
import { MemberDTO } from "../../_models/memberDTO";
import { ActivatedRoute } from "@angular/router";
import { forkJoin } from "rxjs";
import { TranslationsService } from "../../_services/translations.service";
import { Member } from "../../_models/member";

@Component({
  selector: 'app-profile-details',
  templateUrl: './profile-details.component.html',
  styleUrls: ['./profile-details.component.css']
})
// router to /profile/details/{userId}
export class ProfileDetailsComponent implements OnInit {

  constructor(private toastrService: ToastrService,
              private subscriptionsService: SubscriptionsService,
              public translationsService: TranslationsService,
              private route: ActivatedRoute,
              public accountService: AccountService) { }

  currentMember: MemberDTO;

  profileId: string;
  profileMember: Member;

  ngOnInit(): void {
    forkJoin({
      paramMap: this.route.paramMap,
      currMember: this.accountService.currentMember$.pipe(take(1)),
    }).subscribe(({paramMap, currMember}) => {
      console.log("profile details params = ", paramMap);
      paramMap.get('userId') ? this.profileId = paramMap.get('userId') : this.profileId = null;
      if (currMember) {
        this.currentMember = currMember;
      }
      if (this.profileId) {
        this.accountService.getMemberById(this.profileId).pipe(take(1))
          .subscribe((response) => {
            if (response) {
              this.profileMember = response;
            }
          });
      }
    }, (err) => console.log("ERROR ngOnInit forkJoin profile-details:", err));
  }

  addSubscriptionForMember() {
    let addSubDTO: AddSubscriptionDTO = { forLocationId: "", byUserId: this.currentMember.id, forUserId: this.profileMember.id, forCategoryId: "" };
    this.subscriptionsService.addSubscription(addSubDTO).pipe(take(1))
      .subscribe((response) => {
        if (response) {
          this.accountService.refreshCurrentMember(this.currentMember.id);
          this.toastrService.success("Successfully subscribed to " + this.profileMember.id + "'posts");
        } else {
          console.log("add subscription for location returned NULL");
        }
      }, (err) => console.log(err));
  }

}
