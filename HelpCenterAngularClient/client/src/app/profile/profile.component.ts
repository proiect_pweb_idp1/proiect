import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { AuthService } from "@auth0/auth0-angular";
import { switchMap, take, tap } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { AccountService } from "../_services/account.service";
import { RegisterDTO } from "../_models/registerDTO";
import { MemberDTO } from "../_models/memberDTO";
import { Member } from "../_models/member";
import { ToastrService } from "ngx-toastr";
import { FormBuilder, FormGroup, NgForm, Validators } from "@angular/forms";
import { ValidateNumber } from "../_validators/number.validator";
import { BecomeAVolunteerDTO } from "../_models/becomeAVolunteerDTO";
import { TranslationsService } from "../_services/translations.service";
import { TranslationDTO } from "../_models/translationDTO";
import { Observable } from "rxjs";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  myForm = new FormGroup({});

  @HostListener('window:beforeunload', ['$event']) unloadNotification($event: any) {
    if (this.myForm.dirty) {
      $event.returnValue = true;
    }
  }

  validationErrors: string[] = [];

  currentMember: MemberDTO = null;

  constructor(public accountService: AccountService,
              public translationsService: TranslationsService,
              private toastrService: ToastrService,
              private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.setMember();
    this.getTranslations();
  }

  setMember() {
    this.accountService.currentMember$.pipe(take(1))
      .subscribe((member: MemberDTO) => {
        this.currentMember = { ...member };
        this.initializeForm();
      });
  }

  protectedAPICall() {
    this.accountService.protectedAPICall().subscribe(
      (response) => {
        if (response) {
          console.log("protected API CALL SUCCESS", response);
        }
      }, (err) => console.log("ERROR PROTECTED API CALL:", err)
    );
  }

  getTranslations() {
    this.translationsService.translationsSource$.pipe(take(1))
      .subscribe();
  }

  initializeForm() {
    this.myForm = this.fb.group({
      username: [this.currentMember.username, [Validators.required, Validators.minLength(6), Validators.maxLength(30)]],
      age: [this.currentMember.age, [Validators.min(18), Validators.max(120), ValidateNumber()]],
      phoneNumber: [this.currentMember.phoneNumber],
      about: [this.currentMember.about],
    });
  }

  becomeAVolunteer(becomeAVolunteerDTO: BecomeAVolunteerDTO) {
    console.log("becomeAVolunteerDTO:", becomeAVolunteerDTO);
    this.accountService.becomeAVolunteer(becomeAVolunteerDTO).pipe(take(1))
      .subscribe((response) => {
        if (response) {
          console.log("becomeAVolunteer response:", response);
          this.toastrService.info("You are now a Volunteer! Please Log in to see the changes!");
          this.accountService.logout();
        }
      });
  }

  updateMember() {
    this.accountService.updateMember({
      ...this.myForm.value,
      id: this.currentMember.id,
      email: this.currentMember.email,
      lastActive: this.currentMember.lastActive,
      dateCreated: this.currentMember.dateCreated
    } as Member)
      .subscribe((response: Member) => {
        if (response) {
          this.toastrService.success('Profile updated successfully!');
          this.setMember();
        } else {
          console.log("response from Profile UpdateMember is null, error received?");
        }
      }, (error) => this.validationErrors = error);
  }

}
