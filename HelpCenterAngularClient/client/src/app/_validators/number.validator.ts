import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function ValidateNumber(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value = control.value;

    if (value === null) return { invalidNumber: true };

    if (Number.isInteger(parseInt(value.toString()))) {
      return null;
    } else {
      return { invalidNumber: true };
    }
  }
}


