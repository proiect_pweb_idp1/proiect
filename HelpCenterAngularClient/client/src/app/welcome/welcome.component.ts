import { Component, OnInit } from '@angular/core';
import { AccountService } from "../_services/account.service";
import { AuthService } from "@auth0/auth0-angular";
import { MemberDTO } from "../_models/memberDTO";
import { Member } from "../_models/member";
import { map, take } from "rxjs/operators";
import { forkJoin } from "rxjs";

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  member: Member;

  constructor(public accountService: AccountService) {
  }

  // this renders ONLY after Login / Register from Auth0!
  // so we need to call BackEnd API to register the user locally
  ngOnInit(): void {
    const member: MemberDTO = JSON.parse(localStorage.getItem('member'));

    if (!member) {
      this.accountService.register().pipe(take(1)).subscribe((member: Member) => {
        if (member) {
          console.log("welcome page registered returned from API, member:", member);
          forkJoin({
            rolesObj: this.accountService.getRoles().pipe(take(1)),
            accessTokenObj: this.accountService.getAccessTokenSilently().pipe(take(1))
          }).pipe(take(1)).subscribe(({ rolesObj, accessTokenObj }) => {
            console.log("member roles after getRoles:", rolesObj);
            Array.isArray(rolesObj) ? member.roles = rolesObj : member.roles = [];
            console.log("accessToken after getAccessTokenSilently", accessTokenObj);
            this.accountService.setCurrentMember({ ...member, accessToken: accessTokenObj } as MemberDTO);
          }, (error) => console.log("forkJoin error:", error), () => console.log("forkJoin completed"));
        } else {
          console.log("welcome page returned from API null, so we log out");
          this.accountService.logout();
        }
      }, (error) => {
        console.log("error on welcome page register result, so we log out; error:", error);
        this.accountService.logout();
      }, () => console.log("register completed"));
    }
  }

}
