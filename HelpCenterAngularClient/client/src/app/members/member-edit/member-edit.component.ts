import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { AccountService } from "../../_services/account.service";
import { MembersService } from "../../_services/members.service";
import { Member } from "../../_models/member";
import { User } from "../../_models/user";
import { take } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";
import { NgForm } from "@angular/forms";
import { MemberDTO } from "../../_models/memberDTO";

@Component({
  selector: 'app-member-edit',
  templateUrl: './member-edit.component.html',
  styleUrls: ['./member-edit.component.css']
})
export class MemberEditComponent implements OnInit {

  @ViewChild('editForm') editForm: NgForm;

  member: MemberDTO;

  @HostListener('window:beforeunload', ['$event']) unloadNotification($event: any) {
    if (this.editForm.dirty) {
      $event.returnValue = true;
    }
  }

  constructor(private accountService: AccountService, private membersService: MembersService,
              private toastrService: ToastrService) {
    this.accountService.currentMember$.pipe(take(1)).subscribe(m => this.member = m);
  }

  ngOnInit(): void {
    //this.loadMember();
  }

  loadMember() {
    this.membersService.getMember(this.member.username).subscribe(m => this.member = m);
  }

  // updateMember() {
  //   this.membersService.updateMember(this.member).subscribe(() => {
  //     this.toastrService.success('Profile updated succesfully!');
  //     this.editForm.reset(this.member);
  //   });
  // }

}
