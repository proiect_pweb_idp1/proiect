﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace UDEMY_DOTNET_API_6.Migrations
{
    public partial class RemovedmessagesrequestandsomeunusedfieldsfromSubscriptionandPostentities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MessageRequests");

            migrationBuilder.DropColumn(
                name: "ByUsername",
                table: "Subscriptions");

            migrationBuilder.DropColumn(
                name: "ForUsername",
                table: "Subscriptions");

            migrationBuilder.DropColumn(
                name: "PostUsername",
                table: "Posts");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ByUsername",
                table: "Subscriptions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ForUsername",
                table: "Subscriptions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostUsername",
                table: "Posts",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "MessageRequests",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    RecipientId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    SenderId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    RecipientDeleted = table.Column<bool>(type: "bit", nullable: false),
                    RecipientUsername = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SenderDeleted = table.Column<bool>(type: "bit", nullable: false),
                    SenderUsername = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MessageRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MessageRequests_Users_RecipientId",
                        column: x => x.RecipientId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_MessageRequests_Users_SenderId",
                        column: x => x.SenderId,
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_MessageRequests_RecipientId",
                table: "MessageRequests",
                column: "RecipientId");

            migrationBuilder.CreateIndex(
                name: "IX_MessageRequests_SenderId",
                table: "MessageRequests",
                column: "SenderId");
        }
    }
}
