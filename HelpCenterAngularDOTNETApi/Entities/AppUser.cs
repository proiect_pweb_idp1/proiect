﻿namespace UDEMY_DOTNET_API_6.Entities;
public class AppUser
{
    public string Id { get; set; } // referinta la cel din Auth0
    public string Email { get; set; }
    public string UserName { get; set; }
    public int? Age { get; set; }
    public string About { get; set; }
    public string PhoneNumber { get; set; }
    public DateTime DateCreated { get; set; } = DateTime.Now;
    public DateTime LastActive { get; set; } = DateTime.Now;
    public ICollection<Post> Posts { get; set; }
    public ICollection<Subscription> SubscriptionsSent { get; set; }
    public ICollection<Subscription> SubscriptionsReceived { get; set; }
    public ICollection<Message> MessagesSent { get; set; }
    public ICollection<Message> MessagesReceived { get; set; }
    public ICollection<Report> ReportsSent { get; set; }
    public ICollection<Report> ReportsReceived { get; set; }
}
