﻿namespace UDEMY_DOTNET_API_6.Entities;

public class Log
{
    public Guid Id { get; set; }
    public string TableName { get; set; }
    public string Event { get; set; }
    public string Remarks { get; set; }
    public DateTime DateCreated { get; set; }
}
