﻿namespace UDEMY_DOTNET_API_6.Entities;

public class Subscription
{
    public Guid Id { get; set; }
    public string ForUserId { get; set; }
    public AppUser ForUser { get; set; }
    public string ByUserId { get; set; }
    public AppUser ByUser { get; set; }
    public Guid? ForLocationId { get; set; }
    public Location ForLocation { get; set; }
    public Guid? ForCategoryId { get; set; }
    public Category ForCategory { get; set; }
    public DateTime DateCreated { get; set; } = DateTime.UtcNow;
}
