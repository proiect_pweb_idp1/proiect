﻿namespace UDEMY_DOTNET_API_6.Entities;

public class Location
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public bool IsDefault { get; set; }
}
