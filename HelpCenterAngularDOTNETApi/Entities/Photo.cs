﻿namespace UDEMY_DOTNET_API_6.Entities;

public class Photo
{
    public Guid Id { get; set; }
    public string Url { get; set; }
    public string PublicId { get; set; }
    public Post Post { get; set; }
    public Guid PostId { get; set; }
}