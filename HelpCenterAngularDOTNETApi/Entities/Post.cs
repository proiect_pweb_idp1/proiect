﻿namespace UDEMY_DOTNET_API_6.Entities;

public class Post
{
    public Guid Id { get; set; }
    public string Title { get; set; }
    public DateTime DateCreated { get; set; } = DateTime.UtcNow;
    public string Description { get; set; }
    public string PostUserId { get; set; }
    public AppUser PostUser { get; set; }
    public Guid? PostLocationId { get; set; }
    public Location PostLocation { get; set; }
    public Guid? PostCategoryId { get; set; }
    public Category PostCategory { get; set; }
    public ICollection<Photo> Photos { get; set; }
}
