﻿namespace UDEMY_DOTNET_API_6.Entities;

public class Category
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public bool IsDefault { get; set; }
}
