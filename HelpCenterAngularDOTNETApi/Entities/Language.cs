﻿namespace UDEMY_DOTNET_API_6.Entities;

public class Language
{
    public Guid Id { get; set; }
    public string Name { get; set; }
}
