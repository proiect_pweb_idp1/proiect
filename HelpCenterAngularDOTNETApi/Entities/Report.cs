﻿namespace UDEMY_DOTNET_API_6.Entities;

public class Report
{
    public Guid Id { get; set; }
    public string Description { get; set; }
    public DateTime DateCreated { get; set; } = DateTime.UtcNow;
    public string UserReportedId { get; set; }
    public string UserReportedUsername { get; set; }
    public AppUser UserReported { get; set; }
    public string ReportedById { get; set; }
    public string ReportedByUsername { get; set; }
    public AppUser ReportedBy { get; set; }
    public Guid? ReportReasonId { get; set; }
    public Reason ReportReason { get; set; }
}
