﻿namespace UDEMY_DOTNET_API_6.Entities;

public class Translation
{
    public Guid Id { get; set; }
    public Guid LanguageId { get; set; }
    public Language Language { get; set; }
    public string Name { get; set; }
    public string TranslatedName { get; set; }
}
