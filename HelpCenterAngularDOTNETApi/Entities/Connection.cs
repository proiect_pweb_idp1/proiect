﻿namespace UDEMY_DOTNET_API_6.Entities;
public class Connection
{
    public Connection()
    {
    }

    public Connection(string connectionId, string username)
    {
        ConnectionId = connectionId;
        Username = username;
    }

    public string ConnectionId { get; set; } // automatically Primary Key
    public string Username { get; set; }
}