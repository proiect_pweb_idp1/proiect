﻿namespace UDEMY_DOTNET_API_6.Controllers;

[Authorize]
public class SubscriptionsController : BaseApiController
{
    private readonly IUnitOfWork _unitOfWork;

    public SubscriptionsController(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    // api/subscriptions
    [HttpGet]
    public async Task<ActionResult<IEnumerable<SubscriptionDTO>>> GetSubscriptions([FromQuery] SubscriptionParams subParams)
    {
        var subs = await _unitOfWork.SubscriptionsRepository.GetAll(subParams);

        Response.AddPaginationHeader(subs.CurrentPage, subs.PageSize, subs.TotalCount, subs.TotalPages);

        return Ok(subs);
    }

    // api/subscriptions/id
    [HttpGet("{postId}")]
    public async Task<ActionResult<SubscriptionDTO>> GetSubscription(string subscriptionId)
    {
        if (!string.IsNullOrWhiteSpace(subscriptionId))
        {
            return BadRequest("Could not get the subscription!");
        }

        var subscriptionDTO = await _unitOfWork.SubscriptionsRepository.GetSubscription(subscriptionId);

        if (subscriptionDTO == null)
        {
            return BadRequest("Could not get the subscription!");
        }

        return Ok(subscriptionDTO);
    }

    // api/subscriptions
    [HttpPost]
    public async Task<ActionResult<SubscriptionDTO>> AddSubscription(AddSubscriptionDTO addSubscriptionDTO)
    {
        if (User.GetUserId() != addSubscriptionDTO.ByUserId)
        {
            return BadRequest("Internal error, Bad UserId received!");
        }

        var addedSubscriptionId = await _unitOfWork.SubscriptionsRepository.Add(addSubscriptionDTO);

        if (addedSubscriptionId == Guid.Empty)
        {
            return BadRequest("Already subscribed!");
        }

        if (addedSubscriptionId != null && await _unitOfWork.Complete())
        {
            var addedSubscription = await _unitOfWork.SubscriptionsRepository.GetSubscription(addedSubscriptionId.ToString());

            if (addedSubscription != null)
            {
                return Ok(addedSubscription);
            }
        }

        return BadRequest("Internal Error, failed to subscribe!");
    }

    // api/subscriptions
    [HttpDelete]
    public async Task<ActionResult> DeletePost(DeleteSubscriptionDTO deleteSubscriptionDTO)
    {
        if (User.GetUserId() != deleteSubscriptionDTO.ByUserId ||
            (!string.IsNullOrWhiteSpace(deleteSubscriptionDTO.ForUserId) &&
             !await _unitOfWork.UserRepository.UserExists(deleteSubscriptionDTO.ForUserId)))
        {
            return BadRequest("Could not remove the subscription");
        }

        if (await _unitOfWork.SubscriptionsRepository.Delete(deleteSubscriptionDTO))
        {
            var res = await _unitOfWork.Complete();

            if (res)
            {
                return Ok("Subscription removed!");
            }
        }

        return BadRequest("Could not remove the subscription!");
    }

    //// api/users
    //[HttpPut]
    //public async Task<ActionResult> UpdateUser(MemberUpdateDTO memberUpdateDTO)
    //{
    //    var user = await _unitOfWork.UserRepository.GetUserByUsernameAsync(User.GetUsername());

    //    _mapper.Map(memberUpdateDTO, user);

    //    _unitOfWork.UserRepository.Update(user);

    //    if (await _unitOfWork.Complete())
    //    {
    //        return NoContent(); // 204 NoContent, success
    //    }

    //    return BadRequest("Failed to update user!");
    //}

    //[HttpPost("add-photo/{postId}")]
    //public async Task<ActionResult<PhotoDTO>> AddPhoto(string postId, IFormFile file)
    //{
    //    var res = await _unitOfWork.PostsRepository.AddPhoto(postId, file);

    //    if (res)
    //    {
    //        if (await _unitOfWork.Complete())
    //        {
    //            var postDTO = await _unitOfWork.PostsRepository.GetPost(postId);

    //            return Ok(postDTO);
    //        }
    //    }

    //    return BadRequest("Failed to upload photo!");
    //}

    //[HttpPut("set-main-photo/{photoId}")]
    //public async Task<ActionResult> SetMainPhoto(int photoId)
    //{
    //    var user = await _unitOfWork.UserRepository.GetUserByUsernameAsync(User.GetUsername());

    //    var photo = user.Photos.FirstOrDefault(x => x.Id == photoId);

    //    if (photo == null)
    //    {
    //        return NotFound("Photo Id does not exist!");
    //    }

    //    if (photo.IsMain)
    //    {
    //        return BadRequest("This is already your main photo!");
    //    }

    //    var currentMain = user.Photos.FirstOrDefault(x => x.IsMain);

    //    if (currentMain != null)
    //    {
    //        currentMain.IsMain = false;
    //    }

    //    photo.IsMain = true;

    //    if (await _unitOfWork.Complete())
    //    {
    //        return NoContent();
    //    }

    //    return BadRequest("Failed to set main photo");
    //}

    //[HttpDelete("delete-photo/{photoId}")]
    //public async Task<ActionResult<PostDTO>> DeletePhoto(string photoId)
    //{
    //    var post = await _unitOfWork.PostsRepository.GetPostByPhotoId(photoId);

    //    if (post == null)
    //    {
    //        return BadRequest("Could not delete the photo");
    //    }

    //    var res = await _unitOfWork.PostsRepository.DeletePhoto(photoId);

    //    if (!res)
    //    {
    //        return BadRequest("Could not delete the photo");
    //    }

    //    if (await _unitOfWork.Complete())
    //    {
    //        post.Photos = post.Photos.Where(ph => ph.Id != Guid.Parse(photoId)).ToList();

    //        return Ok(post);
    //    }

    //    return BadRequest("Failed to delete the photo");
    //}
}

