﻿namespace UDEMY_DOTNET_API_6.Controllers;
public class AdminController : BaseApiController
{
    //private readonly UserManager<AppUser> _userManager;

    //public AdminController(UserManager<AppUser> userManager)
    //{
    //    _userManager = userManager;
    //}

    //[Authorize("help-center:admin")]
    //[HttpGet("users-with-roles")]
    //public async Task<ActionResult> GetUsersWithRoles()
    //{
    //    var users = await _userManager.Users
    //                    .Include(u => u.UserRoles)
    //                    .ThenInclude(ur => ur.Role)
    //                    .OrderBy(u => u.UserName)
    //                    .Select(u => new
    //                    {
    //                        u.Id,
    //                        Username = u.UserName,
    //                        Roles = u.UserRoles.Select(ur => ur.Role.Name).ToList()
    //                    })
    //                    .ToListAsync();

    //    return Ok(users);
    //}

    //[Authorize("help-center:admin")]
    //[HttpPost("edit-roles/{username}")] // comma separated roles in query params (ex: &roles=admin,member)
    //public async Task<ActionResult> EditRoles(string username, [FromQuery] string roles)
    //{
    //    if (string.IsNullOrWhiteSpace(roles))
    //    {
    //        return BadRequest("Must specify query parameter as a comma separated array of available roles.");
    //    }

    //    // the selected roles for the user
    //    var selectedRoles = roles.Split(",").ToArray();

    //    if (selectedRoles == null || selectedRoles.Count() == 0)
    //    {
    //        return BadRequest("Must specify query parameter as a comma separated array of available roles.");
    //    }

    //    var user = await _userManager.FindByNameAsync(username);

    //    if (user == null)
    //    {
    //        return NotFound("Could not find user.");
    //    }

    //    // current user roles right now
    //    var userRoles = await _userManager.GetRolesAsync(user);

    //    // adds the new roles from the selected ones (the ones that the user is not in them already)
    //    var result = await _userManager.AddToRolesAsync(user, selectedRoles.Except(userRoles));

    //    if (!result.Succeeded)
    //    {
    //        return BadRequest("Failed to add to roles.");
    //    }

    //    result = await _userManager.RemoveFromRolesAsync(user, userRoles.Except(selectedRoles));

    //    if (!result.Succeeded)
    //    {
    //        return BadRequest("Failed to add to roles.");
    //    }

    //    return Ok(await _userManager.GetRolesAsync(user));
    //}

    //[Authorize("help-center:volunteer")]
    //[HttpGet("photos-to-moderate")]
    //public ActionResult GetPhotosForModeration()
    //{
    //    return Ok("Admins or moderators can see this.");
    //}
}
