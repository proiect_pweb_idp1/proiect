﻿namespace UDEMY_DOTNET_API_6.Controllers
{
    [ServiceFilter(typeof(LogUserActivity))]
    [ApiController]
    [Route("api2/[controller]")]
    public class BaseApiWithoutTokenController : ControllerBase
    {

    }
}
