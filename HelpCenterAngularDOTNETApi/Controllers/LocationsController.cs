﻿namespace UDEMY_DOTNET_API_6.Controllers;

public class LocationsController : BaseApiController
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly IConfiguration _config;

    public LocationsController(IUnitOfWork unitOfWork,
                             IConfiguration config,
                             IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
        _config = config;
    }

    [HttpGet]
    public async Task<IEnumerable<LocationDTO>> GetLocations()
    {
        return await _unitOfWork.LocationsRepository.GetAll();
    }
}
