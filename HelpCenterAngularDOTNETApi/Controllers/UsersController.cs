﻿namespace UDEMY_DOTNET_API_6.Controllers;

[Authorize]
public class UsersController : BaseApiController
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly IPhotoService _photoService;

    public UsersController(IUnitOfWork unitOfWork, IMapper mapper, IPhotoService photoService)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
        _photoService = photoService;
    }

    //[HttpGet]
    //public async Task<ActionResult<IEnumerable<MemberDTO>>> GetUsers([FromQuery] UserParams userParams)
    //{
    //    var username = User.GetUsername();

    //    if (string.IsNullOrWhiteSpace(username))
    //    {
    //        return BadRequest("Bad user request. Contact the administrators.");
    //    }

    //    var gender = await _unitOfWork.UserRepository.GetUserGender(username);

    //    if (string.IsNullOrWhiteSpace(gender))
    //    {
    //        return BadRequest("Bad user request. Contact the administrators.");
    //    }

    //    userParams.CurrentUsername = username;

    //    if (string.IsNullOrEmpty(userParams.Gender))
    //    {
    //        userParams.Gender = gender == "male" ? "female" : "male";
    //    }

    //    var users = await _unitOfWork.UserRepository.GetMembersAsync(userParams);

    //    Response.AddPaginationHeader(users.CurrentPage, users.PageSize, users.TotalCount, users.TotalPages);

    //    return Ok(users);
    //}

    // api/users/<username>
    [HttpGet("{username}", Name = "GetUser")]
    public async Task<ActionResult<MemberDTO>> GetUser(string username)
    {
        return await _unitOfWork.UserRepository.GetMemberAsync(username);
    }

    // api/users
    [HttpPut]
    public async Task<ActionResult> UpdateUser(MemberUpdateDTO memberUpdateDTO)
    {
        var user = await _unitOfWork.UserRepository.GetUserByUsernameAsync(User.GetUsername());

        _mapper.Map(memberUpdateDTO, user);

        _unitOfWork.UserRepository.Update(user);

        if (await _unitOfWork.Complete())
        {
            return NoContent(); // 204 NoContent, success
        }

        return BadRequest("Failed to update user!");
    }

    //[HttpPost("add-photo")]
    //public async Task<ActionResult<PhotoDTO>> AddPhoto(IFormFile file)
    //{
    //    var user = await _unitOfWork.UserRepository.GetUserByUsernameAsync(User.GetUsername());

    //    var result = await _photoService.AddPhotoAsync(file);

    //    if (result.Error != null)
    //    {
    //        return BadRequest(result.Error.Message);
    //    }

    //    var photo = new Photo
    //    {
    //        Url = result.SecureUrl.AbsoluteUri,
    //        PublicId = result.PublicId
    //    };

    //    if (user.Photos.Count == 0)
    //    {
    //        photo.IsMain = true;
    //    }

    //    user.Photos.Add(photo);

    //    if (await _unitOfWork.Complete())
    //    {
    //        return CreatedAtRoute("GetUser", new { username = user.UserName }, _mapper.Map<PhotoDTO>(photo));
    //    }

    //    return BadRequest("Problem adding photo");
    //}

    //[HttpPut("set-main-photo/{photoId}")]
    //public async Task<ActionResult> SetMainPhoto(int photoId)
    //{
    //    var user = await _unitOfWork.UserRepository.GetUserByUsernameAsync(User.GetUsername());

    //    var photo = user.Photos.FirstOrDefault(x => x.Id == photoId);

    //    if (photo == null)
    //    {
    //        return NotFound("Photo Id does not exist!");
    //    }

    //    if (photo.IsMain)
    //    {
    //        return BadRequest("This is already your main photo!");
    //    }

    //    var currentMain = user.Photos.FirstOrDefault(x => x.IsMain);

    //    if (currentMain != null)
    //    {
    //        currentMain.IsMain = false;
    //    }

    //    photo.IsMain = true;

    //    if (await _unitOfWork.Complete())
    //    {
    //        return NoContent();
    //    }

    //    return BadRequest("Failed to set main photo");
    //}

    //[HttpDelete("delete-photo/{photoId}")]
    //public async Task<ActionResult> DeletePhoto(int photoId)
    //{
    //    var user = await _unitOfWork.UserRepository.GetUserByUsernameAsync(User.GetUsername());

    //    var photo = user.Photos.FirstOrDefault(x => x.Id == photoId);

    //    if (photo == null)
    //    {
    //        return NotFound();
    //    }

    //    if (photo.IsMain)
    //    {
    //        return BadRequest("You cannot delete your main photo!");
    //    }

    //    if (photo.PublicId != null)
    //    {
    //        var res = await _photoService.DeletePhotoAsync(photo.PublicId);

    //        if (res.Error != null)
    //        {
    //            return BadRequest(res.Error.Message);
    //        }
    //    }

    //    user.Photos.Remove(photo);

    //    if (await _unitOfWork.Complete())
    //    {
    //        return Ok();
    //    }

    //    return BadRequest("Failed to delete the photo");
    //}
}

