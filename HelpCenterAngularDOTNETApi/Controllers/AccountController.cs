﻿namespace UDEMY_DOTNET_API_6.Controllers;

[Authorize]
public class AccountController : BaseApiController
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IManagementAPIService _managementAPIService;
    private readonly IMapper _mapper;
    private readonly IConfiguration _config;

    public AccountController(IUnitOfWork unitOfWork,
                             IManagementAPIService managementAPIService,
                             IConfiguration config,
                             IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _managementAPIService = managementAPIService;
        _mapper = mapper;
        _config = config;
    }

    // api/account/id
    [HttpGet("{memberId}")]
    public async Task<ActionResult<MemberDTO>> GetCurrentMember(string memberId)
    {
        // get the appUser with received memberId and map it to MemberDTO
        var appUserId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;

        if (string.IsNullOrWhiteSpace(appUserId) || string.IsNullOrWhiteSpace(memberId))
        {
            return BadRequest("Bad memberId received!");
        }

        var appUser = await GetUserById(memberId);

        // in case the user is already registered in the local database, normal on auth0 LOGIN
        if (appUser != null)
        {
            // then construct the subscriptionsIds array for MemberDTO
            var mySubscriptionIds = await _unitOfWork.SubscriptionsRepository.GetAllSubscriptionIdsForUserId(memberId);

            var memberDTOToReturn = _mapper.Map<MemberDTO>(appUser);

            memberDTOToReturn.SubscriptionIds = mySubscriptionIds;

            return Ok(memberDTOToReturn);
        }

        return BadRequest("The user with the specified Id does not exist!");
    }

    // This method does the register of the user local profile - is called after EACH register / login from Auth0 FRONTEND
    // so we need to take in consideration every situation and return properly
    [HttpPost("register")]
    public async Task<ActionResult<MemberDTO>> Register(RegisterDTO registerDto)
    {
        var appUserId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;

        if (string.IsNullOrWhiteSpace(appUserId) || string.IsNullOrWhiteSpace(registerDto.Id) || appUserId != registerDto.Id)
        {
            await _unitOfWork.LogsRepository.Add(new Log
            {
                DateCreated = DateTime.UtcNow,
                Id = Guid.NewGuid(),
                TableName = "Users",
                Event = "bad userId received from frontend! does not match access token userId!",
                Remarks = "In frontend, we should logout from auth0 and clear local storage"
            });

            await _unitOfWork.Complete();

            return BadRequest("Bad userId received!");
        }

        var appUser = await GetUserById(registerDto.Id);

        // in case the user is already registered in the local database, normal on auth0 LOGIN
        if (appUser != null)
        {
            // then construct the subscriptionsIds array for MemberDTO
            var mySubscriptionIds = await _unitOfWork.SubscriptionsRepository.GetAllSubscriptionIdsForUserId(appUserId);

            var memberDTOToReturn = _mapper.Map<MemberDTO>(appUser);

            memberDTOToReturn.SubscriptionIds = mySubscriptionIds;

            return memberDTOToReturn;
        }

        // in the case the user is NOT in the database, normal on auth0 REGISTER
        var user = _mapper.Map<AppUser>(registerDto);

        await _unitOfWork.UserRepository.Add(user);

        var result = await _unitOfWork.Complete();

        if (result)
        {
            // then construct the subscriptionsIds array for MemberDTO
            var mySubscriptionIds = await _unitOfWork.SubscriptionsRepository.GetAllSubscriptionIdsForUserId(user.Id);

            var memberDTOToReturn = _mapper.Map<MemberDTO>(user);

            memberDTOToReturn.SubscriptionIds = mySubscriptionIds;

            await _unitOfWork.LogsRepository.Add(new Log
            {
                DateCreated = DateTime.UtcNow,
                Id = Guid.NewGuid(),
                TableName = "Users",
                Event = "Successfully saved the user locally!",
                Remarks = "In frontend, we should render welcome page and store memberDTO in local storage!"
            });

            await _unitOfWork.Complete();

            return memberDTOToReturn;
        }

        // if it couldn't save the user, delete the user from the Auth0 API and then throw exception
        // so that the UI can logout!

        await _managementAPIService.DeleteUser(appUserId);

        await _unitOfWork.LogsRepository.Add(new Log
        {
            DateCreated = DateTime.UtcNow,
            Id = Guid.NewGuid(),
            TableName = "Users",
            Event = "Could not save the user locally!",
            Remarks = "In frontend, we should logout from auth0 and clear local storage. Also, we deleted auth0 user with management API call!"
        });

        await _unitOfWork.Complete();

        return BadRequest("Failed to add user locally!");
    }

    // TO DO: create an endpoint for updating CURRENT user profile's: age, phoneNumber, username, description (about text) !!
    // api/account/update
    [HttpPut("update")]
    public async Task<ActionResult<MemberDTO>> UpdateMyProfile(MemberDTO memberDTO)
    {
        // TO DO UPDATE: VEZI DACA EXISTA PERMISSIONS IN USER.CLAIMS, VEZI DACA E ADMIN
        // SI DACA E ADMIN, MERGE SA FACA UPDATE LA ALT POST DECAT AL LUI

        var user = await _unitOfWork.UserRepository.GetUserByIdAsync(User.GetUserId());

        if (user == null || user.Id != memberDTO.Id || user.Email != memberDTO.Email)
        {
            await _unitOfWork.LogsRepository.Add(new Log
            {
                DateCreated = DateTime.UtcNow,
                Id = Guid.NewGuid(),
                TableName = "Users",
                Event = "Could not update the user locally!",
                Remarks = "User was null or memberDTO Id or Email was different when trying to get it from UserRepository by userId from frontend!"
            });

            await _unitOfWork.Complete();

            return BadRequest("Failed to update user!");
        }

        var appUsers = await _unitOfWork.UserRepository.GetUsersAsync();

        var isUniqueUsername = user.UserName == memberDTO.Username || !appUsers.Select(u => u.UserName).Where(un => un == memberDTO.Username).Any();

        if (string.IsNullOrWhiteSpace(memberDTO.Username) || !isUniqueUsername ||
            (memberDTO.Age != null && (memberDTO.Age < 18 || memberDTO.Age > 120)) ||
            string.IsNullOrWhiteSpace(memberDTO.Username) ||
            memberDTO.Username.Length < 6 || memberDTO.Username.Length > 30)
        {
            await _unitOfWork.LogsRepository.Add(new Log
            {
                DateCreated = DateTime.UtcNow,
                Id = Guid.NewGuid(),
                TableName = "Users",
                Event = "Could not update the user locally!",
                Remarks = "MemberDTO Username received from frontend is NOT UNIQUE or memberDTO received is not valid!"
            });

            await _unitOfWork.Complete();

            return BadRequest("Failed to update user! Username must be unique!");
        }

        _unitOfWork.UserRepository.Detach(user);

        var updatedUser = _mapper.Map<AppUser>(memberDTO);

        _unitOfWork.UserRepository.Update(updatedUser);

        if (await _unitOfWork.Complete())
        {
            await _unitOfWork.LogsRepository.Add(new Log
            {
                DateCreated = DateTime.UtcNow,
                Id = Guid.NewGuid(),
                TableName = "Users",
                Event = "Sucessfully updated the user locally!",
                Remarks = "The frontend should now update its localStorage memberDTO and render new information on the page!"
            });

            await _unitOfWork.Complete();

            return memberDTO;
        }

        await _unitOfWork.LogsRepository.Add(new Log
        {
            DateCreated = DateTime.UtcNow,
            Id = Guid.NewGuid(),
            TableName = "Users",
            Event = "Could not update the user locally!",
            Remarks = "Could not save the update locally, in unitOfWork Complete() method!"
        });

        await _unitOfWork.Complete();

        return BadRequest("Failed to update user!");
    }

    // TO DO: ENDPOINT FOR BECOMING A VOLUNTEER! CALLS MANAGEMENTAPI UPDATEROLE METHOD !!
    // CREATE A DTO THAT CONTAINS USERID AND ROLE both as strings to be called from FRONTEND
    // IN FRONTEND, LOGOUT THE USER SO HE CAN LOG IN AGAIN AND HAVE UPDATED TOKEN AND ROLE
    // MAINTAIN USER PROFILE in accountService currentUser$ observable

    [HttpGet("protectedAPICall")]
    [Authorize("help-center:volunteer")]
    public ActionResult<string> ProtectedVolunteerCallTest()
    {
        return "Authorized";
    }

    [HttpPost("becomeAVolunteer")]
    public async Task<ActionResult<BecomeAVolunteerDTO>> BecomeAVolunteer(BecomeAVolunteerDTO becomeAVolunteerDTO)
    {
        var userExists = await _unitOfWork.UserRepository.UserExists(becomeAVolunteerDTO.Id);

        if (!userExists || User.GetUserId() != becomeAVolunteerDTO.Id)
        {
            BadRequest("Something went wrong! Could not become a volunteer!");
        }

        var res = await _managementAPIService.UpdateUserRole(becomeAVolunteerDTO.Id, _config["VolunteerRoleID"]);

        if (res)
        {
            return becomeAVolunteerDTO;
        }

        return BadRequest("Something went wrong! Could not become a volunteer!");
    }

    //[HttpPost("login")]
    //public async Task<ActionResult<UserDTO>> Login(LoginDTO loginDto)
    //{
    //    var user = await _context.Users
    //        .Include(x => x.Photos)
    //        .SingleOrDefaultAsync(x => x.UserName == loginDto.Username);

    //    if (user == null)
    //    {
    //        return Unauthorized("Invalid username!");
    //    }

    //    using var hmac = new HMACSHA512(user.PasswordSalt);

    //    var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(loginDto.Password));

    //    for (int i = 0; i < computedHash.Length; i++)
    //    {
    //        if (computedHash[i] != user.PasswordHash[i])
    //        {
    //            return Unauthorized("Invalid password!");
    //        }
    //    }

    //    return new UserDTO
    //    {
    //        Username = user.UserName,
    //        Token = _tokenService.CreateToken(user),
    //        PhotoUrl = user.Photos.FirstOrDefault(x => x.IsMain)?.Url,
    //        KnownAs = user.KnownAs,
    //        Gender = user.Gender
    //    };
    //}

    private async Task<AppUser> GetUserById(string id)
    {
        return await _unitOfWork.UserRepository.GetUserByIdAsync(id);
    }
}
