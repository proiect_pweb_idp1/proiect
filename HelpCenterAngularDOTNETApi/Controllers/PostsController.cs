﻿namespace UDEMY_DOTNET_API_6.Controllers;

[Authorize]
public class PostsController : BaseApiController
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly IPhotoService _photoService;

    public PostsController(IUnitOfWork unitOfWork, IMapper mapper, IPhotoService photoService)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
        _photoService = photoService;
    }

    // api/posts
    [HttpGet]
    public async Task<ActionResult<IEnumerable<PostDTO>>> GetPosts([FromQuery] PostParams postParams)
    {
        var posts = await _unitOfWork.PostsRepository.GetAll(postParams);

        Response.AddPaginationHeader(posts.CurrentPage, posts.PageSize, posts.TotalCount, posts.TotalPages);

        return Ok(posts);
    }

    // api/posts/id
    [HttpGet("{postId}")]
    public async Task<ActionResult<PostDTO>> GetPost(string postId)
    {
        var postDTO = await _unitOfWork.PostsRepository.GetPost(postId);

        if (postDTO == null)
        {
            return BadRequest("Could not retrieve post details!");
        }

        return Ok(postDTO);
    }

    // api/posts
    [Authorize("help-center:volunteer")]
    [HttpPost]
    public async Task<ActionResult<PostDTO>> AddPost(AddPostDTO addPostDTO)
    {
        if (User.GetUserId() != addPostDTO.PostUserId)
        {
            return BadRequest("PostUserId differs from User Id!");
        }

        var addedPostId = await _unitOfWork.PostsRepository.Add(addPostDTO);

        if (addedPostId != null && await _unitOfWork.Complete())
        {
            var addedPost = await _unitOfWork.PostsRepository.GetPost(addedPostId.ToString());

            if (addedPost != null)
            {
                return Ok(addedPost);
            }
        }

        return BadRequest("Could not create the post!");
    }

    // api/posts/id
    [Authorize("help-center:volunteer")]
    [HttpPut("{postId}")]
    public async Task<ActionResult<PostDTO>> UpdatePost(string postId, [FromBody] PostDTO postDTO)
    {
        if (string.IsNullOrWhiteSpace(postId) || postId != postDTO.Id.ToString())
        {
            return BadRequest("Incorrect post Ids received!");
        }

        var res = await _unitOfWork.PostsRepository.Update(postDTO);

        if (res)
        {
            if (await _unitOfWork.Complete())
            {
                var response = await _unitOfWork.PostsRepository.GetPost(postId);

                return Ok(response);
            }
        }

        return BadRequest("Could not update post!");
    }

    [Authorize("help-center:volunteer")]
    [HttpDelete("{postId}")]
    public async Task<ActionResult> DeletePost(string postId)
    {
        if (string.IsNullOrWhiteSpace(postId))
        {
            return BadRequest("Invalid postId");
        }

        if (await _unitOfWork.PostsRepository.Delete(postId))
        {
            var res = await _unitOfWork.Complete();

            if (res)
            {
                return Ok("Post deleted!");
            }
        }

        return BadRequest("Could not delete the post!");
    }

    //// api/users
    //[HttpPut]
    //public async Task<ActionResult> UpdateUser(MemberUpdateDTO memberUpdateDTO)
    //{
    //    var user = await _unitOfWork.UserRepository.GetUserByUsernameAsync(User.GetUsername());

    //    _mapper.Map(memberUpdateDTO, user);

    //    _unitOfWork.UserRepository.Update(user);

    //    if (await _unitOfWork.Complete())
    //    {
    //        return NoContent(); // 204 NoContent, success
    //    }

    //    return BadRequest("Failed to update user!");
    //}

    [HttpPost("add-photo/{postId}")]
    public async Task<ActionResult<PhotoDTO>> AddPhoto(string postId, IFormFile file)
    {
        var res = await _unitOfWork.PostsRepository.AddPhoto(postId, file);

        if (res)
        {
            if (await _unitOfWork.Complete())
            {
                var postDTO = await _unitOfWork.PostsRepository.GetPost(postId);

                return Ok(postDTO);
            }
        }

        return BadRequest("Failed to upload photo!");
    }

    //[HttpPut("set-main-photo/{photoId}")]
    //public async Task<ActionResult> SetMainPhoto(int photoId)
    //{
    //    var user = await _unitOfWork.UserRepository.GetUserByUsernameAsync(User.GetUsername());

    //    var photo = user.Photos.FirstOrDefault(x => x.Id == photoId);

    //    if (photo == null)
    //    {
    //        return NotFound("Photo Id does not exist!");
    //    }

    //    if (photo.IsMain)
    //    {
    //        return BadRequest("This is already your main photo!");
    //    }

    //    var currentMain = user.Photos.FirstOrDefault(x => x.IsMain);

    //    if (currentMain != null)
    //    {
    //        currentMain.IsMain = false;
    //    }

    //    photo.IsMain = true;

    //    if (await _unitOfWork.Complete())
    //    {
    //        return NoContent();
    //    }

    //    return BadRequest("Failed to set main photo");
    //}

    [HttpDelete("delete-photo/{photoId}")]
    public async Task<ActionResult<PostDTO>> DeletePhoto(string photoId)
    {
        var post = await _unitOfWork.PostsRepository.GetPostByPhotoId(photoId);

        if (post == null)
        {
            return BadRequest("Could not delete the photo");
        }

        var res = await _unitOfWork.PostsRepository.DeletePhoto(photoId);

        if (!res)
        {
            return BadRequest("Could not delete the photo");
        }

        if (await _unitOfWork.Complete())
        {
            post.Photos = post.Photos.Where(ph => ph.Id != Guid.Parse(photoId)).ToList();

            return Ok(post);
        }

        return BadRequest("Failed to delete the photo");
    }
}

