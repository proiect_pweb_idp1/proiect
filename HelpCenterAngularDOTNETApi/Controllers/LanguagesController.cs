﻿namespace UDEMY_DOTNET_API_6.Controllers;

public class LanguagesController : BaseApiWithoutTokenController
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly IConfiguration _config;

    public LanguagesController(IUnitOfWork unitOfWork,
                             IConfiguration config,
                             IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
        _config = config;
    }

    [HttpGet]
    public async Task<IEnumerable<LanguageDTO>> GetLanguages()
    {
        return await _unitOfWork.LanguagesRepository.GetAll();
    }
}
