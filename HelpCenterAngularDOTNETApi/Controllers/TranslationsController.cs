﻿namespace UDEMY_DOTNET_API_6.Controllers;

public class TranslationsController : BaseApiWithoutTokenController
{
    private readonly IUnitOfWork _unitOfWork;

    public TranslationsController(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    [HttpGet]
    public async Task<IEnumerable<TranslationDTO>> GetTranslations([FromQuery]TranslationsParams translationsParams)
    {
        return await _unitOfWork.TranslationsRepository.GetAll(translationsParams);
    }
}
