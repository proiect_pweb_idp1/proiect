﻿namespace UDEMY_DOTNET_API_6.DTOs;
public class MessageDTO
{
    public Guid Id { get; set; }
    public int SenderId { get; set; }
    public string SenderUsername { get; set; }
    public int RecipientId { get; set; }
    public string RecipientUsername { get; set; }
    public string Content { get; set; }
    public DateTime? DateRead { get; set; }
    public DateTime DateCreated { get; set; }
    [JsonIgnore]
    public bool SenderDeleted { get; set; } // we dont send back this property in the JSON Response Body
    [JsonIgnore]
    public bool RecipientDeleted { get; set; } // we dont send back this property in the JSON Response Body
}

