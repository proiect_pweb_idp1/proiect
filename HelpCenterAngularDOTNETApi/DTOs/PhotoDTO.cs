﻿namespace UDEMY_DOTNET_API_6.DTOs;
public class PhotoDTO
{
    public Guid Id { get; set; }
    public string Url { get; set; }
}