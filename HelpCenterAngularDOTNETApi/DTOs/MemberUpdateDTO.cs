﻿namespace UDEMY_DOTNET_API_6.DTOs;
public class MemberUpdateDTO
{
    public string Username { get; set; }
    public int? Age { get; set; }
    public string About { get; set; }
    public string PhoneNumber { get; set; }
}
