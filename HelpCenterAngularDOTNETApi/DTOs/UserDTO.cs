﻿namespace UDEMY_DOTNET_API_6.DTOs;
public class UserDTO
{
    [Required]
    public string Id { get; set; } // referinta la ID-ul din Auth0
    [Required]
    public string Token { get; set; } // pt. cand se updateaza permisiunile cu ManagementAPI, se intoarce un token refreshed
}
