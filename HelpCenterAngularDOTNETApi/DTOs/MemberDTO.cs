﻿namespace UDEMY_DOTNET_API_6.DTOs;
public class MemberDTO
{
    public string Id { get; set; } // referinta la Id-ul din Auth0
    public string Email { get; set; }
    public string Username { get; set; }
    public int? Age { get; set; }
    public string About { get; set; }
    public string PhoneNumber { get; set; }
    public DateTime DateCreated { get; set; }
    public DateTime LastActive { get; set; }
    public IEnumerable<string> SubscriptionIds { get; set; }
}
