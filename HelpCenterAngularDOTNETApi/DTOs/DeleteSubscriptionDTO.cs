﻿namespace UDEMY_DOTNET_API_6.DTOs
{
    public class DeleteSubscriptionDTO
    {
        public string ByUserId { get; set; }
        public string ForUserId { get; set; }
        public string ForCategoryId { get; set; }
        public string ForLocationId { get; set; }
    }
}
