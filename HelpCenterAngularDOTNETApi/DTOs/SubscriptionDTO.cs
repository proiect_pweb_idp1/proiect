﻿namespace UDEMY_DOTNET_API_6.DTOs;

public class SubscriptionDTO
{
    public Guid Id { get; set; }
    public string ForUserId { get; set; }
    public string ForUsername { get; set; }
    public string ByUserId { get; set; }
    public string ByUsername { get; set; }
    public Guid? ForLocationId { get; set; }
    public string ForLocationName { get; set; }
    public Guid? ForCategoryId { get; set; }
    public string ForCategoryName { get; set; }
    public DateTime DateCreated { get; set; }
}
