﻿namespace UDEMY_DOTNET_API_6.DTOs;

public class ReportDTO
{
    public Guid Id { get; set; }
    public string Description { get; set; }
    public DateTime DateCreated { get; set; }
    public string UserReportedId { get; set; }
    public string UserReportedUsername { get; set; }
    public string ReportedById { get; set; }
    public string ReportedByUsername { get; set; }
    public Guid? ReportReasonId { get; set; }
    public string ReportReasonName { get; set; }
}
