﻿namespace UDEMY_DOTNET_API_6.DTOs;

public class CategoryDTO
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public bool IsDefault { get; set; }
}
