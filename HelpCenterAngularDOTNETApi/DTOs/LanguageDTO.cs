﻿namespace UDEMY_DOTNET_API_6.DTOs;

public class LanguageDTO
{
    public Guid Id { get; set; }
    public string Name { get; set; }
}
