﻿namespace UDEMY_DOTNET_API_6.DTOs
{
    public class AddPostDTO
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string PostUserId { get; set; }
        public string PostLocationId { get; set; }
        public string PostCategoryId { get; set; }
    }
}
