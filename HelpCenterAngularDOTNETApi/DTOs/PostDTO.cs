﻿namespace UDEMY_DOTNET_API_6.DTOs;

public class PostDTO
{
    public Guid Id { get; set; }
    public string Title { get; set; }
    public DateTime DateCreated { get; set; }
    public string Description { get; set; }
    public string PostUserId { get; set; }
    public string PostUsername { get; set; }
    public Guid? PostLocationId { get; set; }
    public string PostLocationName { get; set; }
    public Guid? PostCategoryId { get; set; }
    public string PostCategoryName { get; set; }
    public ICollection<PhotoDTO> Photos { get; set; }
}
