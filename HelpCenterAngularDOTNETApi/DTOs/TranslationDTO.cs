﻿namespace UDEMY_DOTNET_API_6.DTOs;

public class TranslationDTO
{
    public Guid Id { get; set; }
    public Guid LanguageId { get; set; }
    public string LanguageName { get; set; }
    public string Name { get; set; }
    public string TranslatedName { get; set; }
}
