﻿namespace UDEMY_DOTNET_API_6.DTOs;
public class CreateMessageDTO
{
    public string RecipientUsername { get; set; }
    public string Content { get; set; }
}
