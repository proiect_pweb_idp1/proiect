﻿using Auth0.ManagementApi;
using Auth0.ManagementApi.Models;

namespace UDEMY_DOTNET_API_6.Services;

public class ManagementAPIService : IManagementAPIService
{
    private readonly ManagementApiClient _client;
    private readonly string _roleId;

    private readonly IUnitOfWork _unitOfWork;

    public ManagementAPIService(IUnitOfWork unitOfWork, IConfiguration config)
    {
        _unitOfWork = unitOfWork;
        _roleId = config["VolunteerRoleID"];

        // valid 10 days starting from 18 - May - 2022
        _client = new ManagementApiClient(config["Auth0:ManagementAPIToken"], config["Auth0:Domain"]);
    }

    public async Task<bool> UpdateUserRole(string userId, string role)
    {
        if (string.IsNullOrWhiteSpace(userId) || string.IsNullOrWhiteSpace(role) ||
            role != _roleId || !await _unitOfWork.UserRepository.UserExists(userId))
        {
            return false;
        }

        await _client.Users.AssignRolesAsync(userId, new AssignRolesRequest { Roles = new string[] { role } });

        return true;
    }

    public async Task<bool> DeleteUser(string userId)
    {
        if (string.IsNullOrWhiteSpace(userId))
        {
            return false;
        }

        await _client.Users.DeleteAsync(userId);

        return true;
    }
}
