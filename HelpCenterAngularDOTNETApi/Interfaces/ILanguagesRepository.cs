﻿namespace UDEMY_DOTNET_API_6.Interfaces;
public interface ILanguagesRepository
{
    Task<IEnumerable<LanguageDTO>> GetAll();
}
