﻿namespace UDEMY_DOTNET_API_6.Interfaces;
public interface IManagementAPIService
{
    Task<bool> UpdateUserRole(string userId, string role);
    Task<bool> DeleteUser(string userId);
}
