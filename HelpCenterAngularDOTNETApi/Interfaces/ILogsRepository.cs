﻿namespace UDEMY_DOTNET_API_6.Interfaces;
public interface ILogsRepository
{
    Task Add(Log log);
}
