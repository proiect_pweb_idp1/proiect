﻿namespace UDEMY_DOTNET_API_6.Interfaces;
public interface ISubscriptionsRepository
{
    Task<PagedList<SubscriptionDTO>> GetAll(SubscriptionParams subscriptionParams);
    Task<IEnumerable<string>> GetAllSubscriptionIdsForUserId(string memberId);
    Task<SubscriptionDTO> GetSubscription(string subscriptionId);
    Task<Guid?> Add(AddSubscriptionDTO addSubscriptionDTO);
    Task<bool> Delete(DeleteSubscriptionDTO deleteSubscriptionDTO);
}
