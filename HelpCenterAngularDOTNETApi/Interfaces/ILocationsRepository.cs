﻿namespace UDEMY_DOTNET_API_6.Interfaces;
public interface ILocationsRepository
{
    Task<IEnumerable<LocationDTO>> GetAll();
}
