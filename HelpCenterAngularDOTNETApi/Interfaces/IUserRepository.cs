﻿namespace UDEMY_DOTNET_API_6.Interfaces;
public interface IUserRepository
{
    void Update(AppUser user);
    void Detach(AppUser user);
    Task<bool> UserExists(string userId);
    Task Add(AppUser user);
    Task<IEnumerable<AppUser>> GetUsersAsync();
    Task<AppUser> GetUserByIdAsync(string id);
    Task<AppUser> GetUserByUsernameAsync(string username);
    //Task<PagedList<MemberDTO>> GetMembersAsync(UserParams userParams);
    Task<MemberDTO> GetMemberAsync(string username);
    //Task<string> GetUserGender(string username);
}
