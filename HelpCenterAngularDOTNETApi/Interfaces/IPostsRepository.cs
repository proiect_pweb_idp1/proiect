﻿namespace UDEMY_DOTNET_API_6.Interfaces;
public interface IPostsRepository
{
    Task<PagedList<PostDTO>> GetAll(PostParams postParams);
    Task<PostDTO> GetPost(string postId);
    Task<PostDTO> GetPostByPhotoId(string photoId);
    Task<Guid?> Add(AddPostDTO addPostDTO);
    Task<bool> AddPhoto(string postId, IFormFile file);
    Task<bool> DeletePhoto(string photoId);
    Task<bool> Update(PostDTO postDTO);
    Task<bool> Delete(string postId);
}
