﻿namespace UDEMY_DOTNET_API_6.Interfaces;
public interface ITokenService
{
    string CreateToken(AppUser user);
}
