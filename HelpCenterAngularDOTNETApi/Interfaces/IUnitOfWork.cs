﻿namespace UDEMY_DOTNET_API_6.Interfaces;
public interface IUnitOfWork
{
    IUserRepository UserRepository { get; }
    IMessageRepository MessageRepository { get; }
    ILogsRepository LogsRepository { get; }
    ILanguagesRepository LanguagesRepository { get; }
    ITranslationsRepository TranslationsRepository { get; }
    ILocationsRepository LocationsRepository { get; }
    ICategoriesRepository CategoriesRepository { get; }
    IPostsRepository PostsRepository { get; }
    ISubscriptionsRepository SubscriptionsRepository { get; }

    // save all our changes
    Task<bool> Complete();
    // helper method -- see if EF has any changes
    bool HasChanges();
}
