﻿namespace UDEMY_DOTNET_API_6.Interfaces;
public interface ITranslationsRepository
{
    Task<IEnumerable<TranslationDTO>> GetAll(TranslationsParams translationsParams);
}
