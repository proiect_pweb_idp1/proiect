﻿namespace UDEMY_DOTNET_API_6.Interfaces;
public interface ICategoriesRepository
{
    Task<IEnumerable<CategoryDTO>> GetAll();
}
