﻿namespace UDEMY_DOTNET_API_6.Data;

public class SubscriptionsRepository : ISubscriptionsRepository
{
    private readonly DataContext _context;
    private readonly IMapper _mapper;

    public SubscriptionsRepository(DataContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<SubscriptionDTO> GetSubscription(string subscriptionId)
    {
        if (string.IsNullOrWhiteSpace(subscriptionId))
        {
            return null;
        }

        var subscription = await _context.Subscriptions.Include(s => s.ForLocation).Include(s => s.ForCategory)
                                               .Include(s => s.ForUser).Include(s => s.ByUser)
                                .FirstOrDefaultAsync(s => s.Id == Guid.Parse(subscriptionId));

        if (subscription == null)
        {
            return null;
        }

        return _mapper.Map<SubscriptionDTO>(subscription);
    }

    public async Task<IEnumerable<string>> GetAllSubscriptionIdsForUserId(string userId)
    {
        var subsIdsForOtherUsers = await _context.Subscriptions.AsNoTracking()
                                                .Where(s => s.ByUserId == userId && !string.IsNullOrWhiteSpace(s.ForUserId)).Select(s => s.ForUserId)
                                                .ToListAsync();
        var subsIdsForCategories = await _context.Subscriptions.AsNoTracking()
                                                .Where(s => s.ByUserId == userId && s.ForCategoryId != null).Select(s => s.ForCategoryId.ToString())
                                                .ToListAsync();
        var subsIdsForLocations = await _context.Subscriptions.AsNoTracking()
                                                .Where(s => s.ByUserId == userId && s.ForLocationId != null).Select(s => s.ForLocationId.ToString())
                                                .ToListAsync();
        var concatListOfIdsForSubs = new List<string>();

        if (subsIdsForCategories != null) concatListOfIdsForSubs.AddRange(subsIdsForCategories);
        if (subsIdsForLocations != null) concatListOfIdsForSubs.AddRange(subsIdsForLocations);
        if (subsIdsForOtherUsers != null) concatListOfIdsForSubs.AddRange(subsIdsForOtherUsers);

        return concatListOfIdsForSubs;
    }

    public async Task<PagedList<SubscriptionDTO>> GetAll(SubscriptionParams subscriptionParams)
    {
        var query = _context.Subscriptions.Include(s => s.ForUser).Include(s => s.ByUser)
                                          .Include(s => s.ForLocation).Include(s => s.ForCategory).AsQueryable();

        if (!string.IsNullOrWhiteSpace(subscriptionParams.CategoryId))
        {
            query = query.Where(s => s.ForCategoryId == Guid.Parse(subscriptionParams.CategoryId));
        }

        if (!string.IsNullOrWhiteSpace(subscriptionParams.LocationId))
        {
            query = query.Where(s => s.ForLocationId == Guid.Parse(subscriptionParams.LocationId));
        }

        if (!string.IsNullOrWhiteSpace(subscriptionParams.OrderBy) && (subscriptionParams.OrderBy == "Newest" || subscriptionParams.OrderBy == "Oldest"))
        {
            query = subscriptionParams.OrderBy switch
            {
                "Newest" => query.OrderByDescending(s => s.DateCreated),
                _ => query.OrderBy(s => s.DateCreated)
            };
        }

        if (!string.IsNullOrWhiteSpace(subscriptionParams.ForUsername))
        {
            var postUser = await _context.Users.AsNoTracking().Where(u => u.UserName == subscriptionParams.ForUsername).FirstOrDefaultAsync();

            if (postUser != null)
            {
                // user exists
                query = query.Where(s => s.ForUserId == postUser.Id);
            }
        }

        if (!string.IsNullOrWhiteSpace(subscriptionParams.ByUsername))
        {
            var postUser = await _context.Users.AsNoTracking().Where(u => u.UserName == subscriptionParams.ByUsername).FirstOrDefaultAsync();

            if (postUser != null)
            {
                // user exists
                query = query.Where(s => s.ByUserId == postUser.Id);
            }
        }

        return await PagedList<SubscriptionDTO>.CreateAsync(
                query.ProjectTo<SubscriptionDTO>(_mapper.ConfigurationProvider)
                     .AsNoTracking(),
                subscriptionParams.PageNumber, subscriptionParams.PageSize);
    }

    public async Task<Guid?> Add(AddSubscriptionDTO addSubscriptionDTO)
    {
        if (!string.IsNullOrWhiteSpace(addSubscriptionDTO.ForCategoryId))
        {
            if (await _context.Subscriptions.AsNoTracking().Where(s => s.ByUserId == addSubscriptionDTO.ByUserId &&
                                                                       s.ForCategoryId == Guid.Parse(addSubscriptionDTO.ForCategoryId))
                                    .AnyAsync())
            {
                return Guid.Empty; // already exists one subscription!
            }
        }

        if (!string.IsNullOrWhiteSpace(addSubscriptionDTO.ForLocationId))
        {
            if (await _context.Subscriptions.AsNoTracking().Where(s => s.ByUserId == addSubscriptionDTO.ByUserId &&
                                                                       s.ForLocationId == Guid.Parse(addSubscriptionDTO.ForLocationId))
                                    .AnyAsync())
            {
                return Guid.Empty; // already exists one subscription!
            }
        }

        if (!string.IsNullOrWhiteSpace(addSubscriptionDTO.ForUserId))
        {
            if (await _context.Subscriptions.AsNoTracking().Where(s => s.ByUserId == addSubscriptionDTO.ByUserId &&
                                                                       s.ForUserId == addSubscriptionDTO.ForUserId)
                                    .AnyAsync())
            {
                return Guid.Empty; // already exists one subscription!
            }
        }

        var subscription = _mapper.Map<Subscription>(addSubscriptionDTO);

        if (subscription != null)
        {
            subscription.Id = Guid.NewGuid();
            subscription.DateCreated = DateTime.UtcNow;
        }
        else
        {
            return null;
        }

        if (subscription.ForLocationId == Guid.Empty)
        {
            subscription.ForLocationId = null;
        }

        if (subscription.ForCategoryId == Guid.Empty)
        {
            subscription.ForCategoryId = null;
        }

        await _context.Subscriptions.AddAsync(subscription);

        return subscription.Id;
    }

    // delete
    public async Task<bool> Delete(DeleteSubscriptionDTO deleteSubscriptionDTO)
    {
        if (!string.IsNullOrWhiteSpace(deleteSubscriptionDTO.ForCategoryId) && string.IsNullOrWhiteSpace(deleteSubscriptionDTO.ForUserId) &&
            string.IsNullOrWhiteSpace(deleteSubscriptionDTO.ForLocationId))
        {
            if (await _context.Categories.AsNoTracking().FirstOrDefaultAsync(c => c.Id == Guid.Parse(deleteSubscriptionDTO.ForCategoryId)) != null)
            {
                var sub = await _context.Subscriptions
                            .Where(s => s.ForCategoryId == Guid.Parse(deleteSubscriptionDTO.ForCategoryId) &&
                                        s.ByUserId == deleteSubscriptionDTO.ByUserId)
                            .FirstOrDefaultAsync();

                if (sub != null)
                {
                    _context.Subscriptions.Remove(sub);

                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        if (!string.IsNullOrWhiteSpace(deleteSubscriptionDTO.ForLocationId) && string.IsNullOrWhiteSpace(deleteSubscriptionDTO.ForUserId) &&
            string.IsNullOrWhiteSpace(deleteSubscriptionDTO.ForCategoryId))
        {
            if (await _context.Locations.AsNoTracking().FirstOrDefaultAsync(l => l.Id == Guid.Parse(deleteSubscriptionDTO.ForLocationId)) != null)
            {
                var sub = await _context.Subscriptions
                            .Where(s => s.ForLocationId == Guid.Parse(deleteSubscriptionDTO.ForLocationId) &&
                                        s.ByUserId == deleteSubscriptionDTO.ByUserId)
                            .FirstOrDefaultAsync();

                if (sub != null)
                {
                    _context.Subscriptions.Remove(sub);

                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        if (!string.IsNullOrWhiteSpace(deleteSubscriptionDTO.ForUserId) && string.IsNullOrWhiteSpace(deleteSubscriptionDTO.ForLocationId) &&
            string.IsNullOrWhiteSpace(deleteSubscriptionDTO.ForCategoryId))
        {
            if (await _context.Users.AsNoTracking().FirstOrDefaultAsync(u => u.Id == deleteSubscriptionDTO.ForUserId) != null)
            {
                var sub = await _context.Subscriptions
                            .Where(s => s.ForUserId == deleteSubscriptionDTO.ForCategoryId &&
                                        s.ByUserId == deleteSubscriptionDTO.ByUserId)
                            .FirstOrDefaultAsync();

                if (sub != null)
                {
                    _context.Subscriptions.Remove(sub);

                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        return false;
    }
}
