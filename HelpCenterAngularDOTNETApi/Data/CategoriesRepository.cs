﻿namespace UDEMY_DOTNET_API_6.Data;
public class CategoriesRepository : ICategoriesRepository
{
    private readonly DataContext _context;
    private readonly IMapper _mapper;

    public CategoriesRepository(DataContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<IEnumerable<CategoryDTO>> GetAll()
    {
        return await _context.Categories.AsNoTracking().Select(c => _mapper.Map<CategoryDTO>(c)).ToListAsync();
    }
}
