﻿namespace UDEMY_DOTNET_API_6.Data;
public class TranslationsRepository : ITranslationsRepository
{
    private readonly DataContext _context;
    private readonly IMapper _mapper;

    public TranslationsRepository(DataContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<IEnumerable<TranslationDTO>> GetAll(TranslationsParams translationsParams)
    {
        var result = await _context.Translations.AsNoTracking().Include(t => t.Language)
                    .Where(t => t.Language.Id == Guid.Parse(translationsParams.LanguageId)).Select(t => _mapper.Map<TranslationDTO>(t))
                    .ToListAsync();

        return result;
    }
}
