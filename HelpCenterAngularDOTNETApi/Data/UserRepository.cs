﻿namespace UDEMY_DOTNET_API_6.Data;
public class UserRepository : IUserRepository
{
    private readonly DataContext _context;
    private readonly IMapper _mapper;

    public UserRepository(DataContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public void Detach(AppUser appUser)
    {
        if (appUser != null)
        {
            _context.Entry(appUser).State = EntityState.Detached;
        }
    }

    public async Task<bool> UserExists(string userId)
    {
        if (string.IsNullOrWhiteSpace(userId))
        {
            return false;
        }

        return await _context.Users.Where(u => u.Id == userId).AnyAsync();
    }

    public async Task<MemberDTO> GetMemberAsync(string username)
    {
        return await _context.Users
            .Where(x => x.UserName == username)
            .ProjectTo<MemberDTO>(_mapper.ConfigurationProvider)
            .SingleOrDefaultAsync();
    }

    // TO DO: ca model pt. celelalte entitati

    //public async Task<PagedList<MemberDTO>> GetMembersAsync(UserParams userParams)
    //{
    //    var query = _context.Users
    //        .AsQueryable();

    //    query = query.Where(u => u.UserName != userParams.CurrentUsername);
    //    query = query.Where(u => u.Gender == userParams.Gender);

    //    var minDob = DateTime.Today.AddYears(-userParams.MaxAge - 1);
    //    var maxDob = DateTime.Today.AddYears(-userParams.MinAge);

    //    query = query.Where(u => u.DateOfBirth >= minDob && u.DateOfBirth <= maxDob);

    //    query = userParams.OrderBy switch
    //    {
    //        "created" => query.OrderByDescending(u => u.Created),
    //        _ => query.OrderByDescending(u => u.LastActive)
    //    };

    //    return await PagedList<MemberDTO>.CreateAsync(
    //            query.ProjectTo<MemberDTO>(_mapper.ConfigurationProvider)
    //                 .AsNoTracking(),
    //            userParams.PageNumber, userParams.PageSize);
    //}

    public async Task<AppUser> GetUserByIdAsync(string id)
    {
        return await _context.Users
            .FindAsync(id);
    }

    public async Task<AppUser> GetUserByUsernameAsync(string username)
    {
        return await _context.Users
            .Include(u => u.Posts)
            .SingleOrDefaultAsync(x => x.UserName == username);
    }

    public async Task<IEnumerable<AppUser>> GetUsersAsync()
    {
        return await _context.Users
            .Include(u => u.Posts)
            .ToListAsync();
    }

    public async Task Add(AppUser user)
    {
        await _context.Users.AddAsync(user);
    }

    public void Update(AppUser user)
    {
        // marks the entity with a MODIFIED flag (the entity has been modified)
        _context.Entry(user).State = EntityState.Modified;
    }
}
