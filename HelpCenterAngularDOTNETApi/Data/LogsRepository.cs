﻿namespace UDEMY_DOTNET_API_6.Data;
public class LogsRepository : ILogsRepository
{
    private readonly DataContext _context;

    public LogsRepository(DataContext context)
    {
        _context = context;
    }

    public async Task Add(Log log)
    {
        await _context.Logs.AddAsync(log);
    }
}
