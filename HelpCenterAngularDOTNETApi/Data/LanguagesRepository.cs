﻿namespace UDEMY_DOTNET_API_6.Data;
public class LanguagesRepository : ILanguagesRepository
{
    private readonly DataContext _context;
    private readonly IMapper _mapper;

    public LanguagesRepository(DataContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<IEnumerable<LanguageDTO>> GetAll()
    {
        return await _context.Languages.Select(l => _mapper.Map<LanguageDTO>(l)).ToListAsync();
    }
}
