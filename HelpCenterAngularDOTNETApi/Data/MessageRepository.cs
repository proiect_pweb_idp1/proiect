﻿namespace UDEMY_DOTNET_API_6.Data;

public class MessageRepository : IMessageRepository
{
    private readonly DataContext _context;
    private readonly IMapper _mapper;

    public MessageRepository(DataContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public void AddGroup(Group group)
    {
        _context.Groups.Add(group);
    }

    public void AddMessage(Message message)
    {
        _context.Messages.Add(message);
    }

    public void DeleteMessage(Message message)
    {
        _context.Messages.Remove(message);
    }

    public async Task<Connection> GetConnection(string connectionId)
    {
        return await _context.Connections.FindAsync(connectionId);
    }

    public async Task<Group> GetGroupForConnection(string connectionId)
    {
        return await _context.Groups.Include(g => g.Connections)
                    .Where(g => g.Connections.Any(c => c.ConnectionId == connectionId))
                    .FirstOrDefaultAsync();
    }

    public async Task<Message> GetMessage(Guid id)
    {
        return await _context.Messages.Include(m => m.Sender).Include(m => m.Recipient)
                    .SingleOrDefaultAsync(m => m.Id == id);
    }

    public async Task<Group> GetMessageGroup(string groupName)
    {
        return await _context.Groups
            .Include(x => x.Connections)
            .FirstOrDefaultAsync(x => x.Name == groupName);
    }

    // Gets the UNDELETED (active) messages a user has SENT / RECEIVED
    public async Task<PagedList<MessageDTO>> GetMessagesForUser(MessageParams messageParams)
    {
        var query = _context.Messages
                        .OrderByDescending(m => m.DateCreated)
                        .ProjectTo<MessageDTO>(_mapper.ConfigurationProvider)
                        .AsQueryable();

        query = messageParams.Container switch
        {
            "Inbox" => query.Where(m => m.RecipientUsername == messageParams.Username &&
                                        m.RecipientDeleted == false), // we are the recipient of the message, our INBOX
            "Outbox" => query.Where(m => m.SenderUsername == messageParams.Username &&
                                         m.SenderDeleted == false), // we are the sender of the message, our OUTBOX
            _ => query.Where(m => m.RecipientUsername == messageParams.Username &&
                                    m.DateRead == null && m.RecipientDeleted == false)// default case, our UNREAD INBOX messages
        };

        return await PagedList<MessageDTO>.CreateAsync(query, messageParams.PageNumber, messageParams.PageSize);
    }

    // Gets all the UNDELETED(active) messages between two users (the current user and a recipient).
    // Marks as read all the received messages that were unread before, for the current user
    public async Task<IEnumerable<MessageDTO>> GetMessageThread(string currentUsername, string recipientUsername)
    {
        var messages = await _context.Messages
                            .Where(m => (m.Recipient.UserName == currentUsername && m.RecipientDeleted == false &&
                                         m.Sender.UserName == recipientUsername) ||
                                        (m.Recipient.UserName == recipientUsername &&
                                         m.Sender.UserName == currentUsername && m.SenderDeleted == false))
                            .OrderBy(m => m.DateCreated)
                            .ProjectTo<MessageDTO>(_mapper.ConfigurationProvider) // we dont need to include anything, ProjectTo will include needed entities related
                            .ToListAsync();

        var unreadMessages = messages.Where(m => m.DateRead == null &&
                                                 m.RecipientUsername == currentUsername)
                                           .ToList();

        if (unreadMessages.Any())
        {
            foreach (var m in unreadMessages)
            {
                m.DateRead = DateTime.UtcNow;
            }
        }

        return messages;
    }

    public void RemoveConnection(Connection connection)
    {
        _context.Connections.Remove(connection);
    }
}
