﻿namespace UDEMY_DOTNET_API_6.Data;
public class Seed
{
    public static async Task SeedAll(DataContext context, IMapper mapper)
    {
        await SeedLanguages(context);
        await SeedTranslations(context, mapper);
        await SeedReasons(context);
        await SeedLocations(context);
        await SeedCategories(context);
    }
    public static async Task SeedLanguages(DataContext context)
    {
        var languageData = await File.ReadAllTextAsync("Data/Languages.json");

        var languages = JsonSerializer.Deserialize<List<Language>>(languageData);

        foreach (var language in languages)
        {
            if (await context.Languages.Where(l => l.Name == language.Name).AnyAsync())
            {
                continue;
            }

            language.Id = Guid.NewGuid();

            context.Languages.Add(language);
        }

        await context.SaveChangesAsync();
    }
    public static async Task SeedTranslations(DataContext context, IMapper mapper)
    {
        var translationsData = await File.ReadAllTextAsync("Data/Translations.json");

        var translations = JsonSerializer.Deserialize<List<TranslationDTO>>(translationsData);

        foreach (var translationDTO in translations)
        {
            if (await context.Translations.Include(t => t.Language).Where(t => t.Language.Name == translationDTO.LanguageName &&
                                                                                t.Name == translationDTO.Name)
                        .AnyAsync())
            {
                continue;
            }

            var translation = mapper.Map<Translation>(translationDTO);

            translation.Id = Guid.NewGuid();
            translation.LanguageId = await context.Languages.Where(l => l.Name == translationDTO.LanguageName)
                                                            .Select(l => l.Id)
                                                            .FirstOrDefaultAsync();

            context.Translations.Add(translation);
        }

        await context.SaveChangesAsync();
    }
    public static async Task SeedReasons(DataContext context)
    {
        var reasonsData = await File.ReadAllTextAsync("Data/Reasons.json");

        var reasons = JsonSerializer.Deserialize<List<Reason>>(reasonsData);

        foreach (var reason in reasons)
        {
            if (await context.Reasons.Where(r => r.Name == reason.Name).AnyAsync())
            {
                continue;
            }

            reason.Id = Guid.NewGuid();

            context.Reasons.Add(reason);
        }

        await context.SaveChangesAsync();
    }
    public static async Task SeedLocations(DataContext context)
    {
        var locationsData = await File.ReadAllTextAsync("Data/Locations.json");

        var locations = JsonSerializer.Deserialize<List<Location>>(locationsData);

        foreach (var location in locations)
        {
            if (await context.Locations.Where(l => l.Name == location.Name).AnyAsync())
            {
                continue;
            }

            location.Id = Guid.NewGuid();
            location.IsDefault = true;

            context.Locations.Add(location);
        }

        await context.SaveChangesAsync();
    }
    public static async Task SeedCategories(DataContext context)
    {
        var categoriesData = await File.ReadAllTextAsync("Data/Categories.json");

        var categories = JsonSerializer.Deserialize<List<Category>>(categoriesData);

        foreach (var category in categories)
        {
            if (await context.Categories.Where(c => c.Name == category.Name).AnyAsync())
            {
                continue;
            }

            category.Id = Guid.NewGuid();
            category.IsDefault = true;

            context.Categories.Add(category);
        }

        await context.SaveChangesAsync();
    }

    //public static async Task SeedUsers(UserManager<AppUser> userManager,
    //                RoleManager<AppRole> roleManager)
    //{
    //    if (await userManager.Users.AnyAsync()) return;

    //    var userData = await File.ReadAllTextAsync("Data/UserSeedData.json");

    //    var users = JsonSerializer.Deserialize<List<AppUser>>(userData);

    //    if (users == null) return;

    //    var roles = new List<AppRole>()
    //        {
    //            new AppRole{ Name = "Member" },
    //            new AppRole{ Name = "Admin" },
    //            new AppRole{ Name = "Moderator" },
    //        };

    //    foreach (var role in roles)
    //    {
    //        await roleManager.CreateAsync(role);
    //    }

    //    foreach (var user in users)
    //    {
    //        user.UserName = user.UserName.ToLower();

    //        await userManager.CreateAsync(user, "Pa$$w0rd");

    //        await userManager.AddToRoleAsync(user, "Member");
    //    }

    //    var admin = new AppUser
    //    {
    //        UserName = "admin",
    //    };

    //    await userManager.CreateAsync(admin, "Pa$$w0rd");

    //    await userManager.AddToRolesAsync(admin, new[] { "Admin", "Moderator" });
    //}
}
