﻿namespace UDEMY_DOTNET_API_6.Data;

public class DataContext : DbContext
{
    public DataContext(DbContextOptions options) : base(options)
    {
    }

    public DbSet<AppUser> Users { get; set; }
    public DbSet<Post> Posts { get; set; }
    public DbSet<Photo> Photos { get; set; }
    public DbSet<Message> Messages { get; set; }
    public DbSet<Subscription> Subscriptions { get; set; }
    public DbSet<Report> Reports { get; set; }
    public DbSet<Reason> Reasons { get; set; }
    public DbSet<Location> Locations { get; set; }
    public DbSet<Category> Categories { get; set; }
    public DbSet<Language> Languages { get; set; }
    public DbSet<Translation> Translations { get; set; }
    public DbSet<Log> Logs { get; set; }
    public DbSet<Group> Groups { get; set; }
    public DbSet<Connection> Connections { get; set; }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder); // super-class call

        builder.Entity<Post>()
            .HasOne(p => p.PostUser)
            .WithMany(u => u.Posts)
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired();

        builder.Entity<Post>()
            .HasOne(p => p.PostLocation)
            .WithMany()
            .OnDelete(DeleteBehavior.SetNull)
            .IsRequired(false);

        builder.Entity<Post>()
            .HasOne(p => p.PostCategory)
            .WithMany()
            .OnDelete(DeleteBehavior.SetNull)
            .IsRequired(false);

        builder.Entity<Post>()
            .HasMany(p => p.Photos)
            .WithOne(ph => ph.Post)
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired();

        builder.Entity<Message>()
            .HasOne(m => m.Recipient)
            .WithMany(u => u.MessagesReceived)
            .OnDelete(DeleteBehavior.SetNull)
            .IsRequired(false);

        builder.Entity<Message>()
            .HasOne(m => m.Sender)
            .WithMany(u => u.MessagesSent)
            .OnDelete(DeleteBehavior.NoAction)
            .IsRequired(false);

        builder.Entity<Subscription>()
            .HasOne(s => s.ForUser)
            .WithMany(u => u.SubscriptionsReceived)
            .OnDelete(DeleteBehavior.NoAction)
            .IsRequired();

        builder.Entity<Subscription>()
            .HasOne(s => s.ByUser)
            .WithMany(u => u.SubscriptionsSent)
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired();

        builder.Entity<Subscription>()
            .HasOne(s => s.ForLocation)
            .WithMany()
            .OnDelete(DeleteBehavior.SetNull)
            .IsRequired(false);

        builder.Entity<Subscription>()
            .HasOne(s => s.ForCategory)
            .WithMany()
            .OnDelete(DeleteBehavior.SetNull)
            .IsRequired(false);

        builder.Entity<Report>()
            .HasOne(mr => mr.UserReported)
            .WithMany(u => u.ReportsReceived)
            .OnDelete(DeleteBehavior.NoAction)
            .IsRequired();

        builder.Entity<Report>()
            .HasOne(mr => mr.ReportedBy)
            .WithMany(u => u.ReportsSent)
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired();

        builder.Entity<Report>()
            .HasOne(mr => mr.ReportReason)
            .WithMany()
            .OnDelete(DeleteBehavior.SetNull)
            .IsRequired(false);

        builder.Entity<Translation>()
            .HasOne(t => t.Language)
            .WithMany()
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired();

        builder.ApplyUtcDateTimeConverter(); // after all our entities configurations
    }
}

// this is used for converting all the DateTime and DateTime? (nullable DateTime) types to Utc format
// for persistance storage in the database, in Utc format
public static class UtcDateAnnotation
{
    private const string IsUtcAnnotation = "IsUtc";
    private static readonly ValueConverter<DateTime, DateTime> UtcConverter =
      new(v => v, v => DateTime.SpecifyKind(v, DateTimeKind.Utc));

    private static readonly ValueConverter<DateTime?, DateTime?> UtcNullableConverter =
      new(v => v, v => v == null ? v : DateTime.SpecifyKind(v.Value, DateTimeKind.Utc));

    public static PropertyBuilder<TProperty> IsUtc<TProperty>(this PropertyBuilder<TProperty> builder, bool isUtc = true) =>
      builder.HasAnnotation(IsUtcAnnotation, isUtc);

    public static bool IsUtc(this IMutableProperty property) =>
      ((bool?)property.FindAnnotation(IsUtcAnnotation)?.Value) ?? true;

    /// <summary>
    /// Make sure this is called after configuring all your entities.
    /// </summary>
    public static void ApplyUtcDateTimeConverter(this ModelBuilder builder)
    {
        foreach (var entityType in builder.Model.GetEntityTypes())
        {
            foreach (var property in entityType.GetProperties())
            {
                if (!property.IsUtc())
                {
                    continue;
                }

                if (property.ClrType == typeof(DateTime))
                {
                    property.SetValueConverter(UtcConverter);
                }

                if (property.ClrType == typeof(DateTime?))
                {
                    property.SetValueConverter(UtcNullableConverter);
                }
            }
        }
    }
}

