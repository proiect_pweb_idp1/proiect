﻿namespace UDEMY_DOTNET_API_6.Data;
public class UnitOfWork : IUnitOfWork
{
    private readonly DataContext _context;
    private readonly IPhotoService _photoService;
    private readonly IMapper _mapper;

    public UnitOfWork(DataContext context, IMapper mapper, IPhotoService photoService)
    {
        _context = context;
        _mapper = mapper;
        _photoService = photoService;
    }

    public IUserRepository UserRepository => new UserRepository(_context, _mapper);
    public IMessageRepository MessageRepository => new MessageRepository(_context, _mapper);
    public ILogsRepository LogsRepository => new LogsRepository(_context);
    public ILanguagesRepository LanguagesRepository => new LanguagesRepository(_context, _mapper);
    public ITranslationsRepository TranslationsRepository => new TranslationsRepository(_context, _mapper);
    public ILocationsRepository LocationsRepository => new LocationsRepository(_context, _mapper);
    public ICategoriesRepository CategoriesRepository => new CategoriesRepository(_context, _mapper);
    public IPostsRepository PostsRepository => new PostsRepository(_context, _photoService, _mapper);
    public ISubscriptionsRepository SubscriptionsRepository => new SubscriptionsRepository(_context, _mapper);

    public async Task<bool> Complete()
    {
        return await _context.SaveChangesAsync() > 0;
    }

    public bool HasChanges()
    {
        return _context.ChangeTracker.HasChanges();
    }
}
