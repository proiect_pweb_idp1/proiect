﻿namespace UDEMY_DOTNET_API_6.Data;

public class PostsRepository : IPostsRepository
{
    private readonly DataContext _context;
    private readonly IPhotoService _photoService;
    private readonly IMapper _mapper;

    public PostsRepository(DataContext context, IPhotoService photoService, IMapper mapper)
    {
        _context = context;
        _photoService = photoService;
        _mapper = mapper;
    }

    public async Task<PostDTO> GetPost(string postId)
    {
        if (string.IsNullOrWhiteSpace(postId))
        {
            return null;
        }

        var post = await _context.Posts.Include(p => p.Photos).Include(p => p.PostUser).Include(p => p.PostLocation).Include(p => p.PostCategory)
            .FirstOrDefaultAsync(p => p.Id == Guid.Parse(postId));

        if (post == null)
        {
            return null;
        }

        return _mapper.Map<PostDTO>(post);
    }

    public async Task<PostDTO> GetPostByPhotoId(string photoId)
    {
        var posts = _context.Posts.Include(p => p.Photos).AsNoTracking().AsEnumerable();
    
        var postId = posts.Where(p => p.Photos.Where(ph => ph.Id == Guid.Parse(photoId)).Any())
                          .Select(p => p.Id).First();

        var postDTO = await GetPost(postId.ToString());

        return postDTO;
    }

    public async Task<PagedList<PostDTO>> GetAll(PostParams postParams)
    {
        var query = _context.Posts.Include(p => p.Photos).Include(p => p.PostLocation).Include(p => p.PostCategory).Include(p => p.PostUser).AsQueryable();

        if (!string.IsNullOrWhiteSpace(postParams.CategoryId))
        {
            query = query.Where(p => p.PostCategoryId == Guid.Parse(postParams.CategoryId));
        }

        if (!string.IsNullOrWhiteSpace(postParams.LocationId))
        {
            query = query.Where(p => p.PostLocationId == Guid.Parse(postParams.LocationId));
        }

        if (!string.IsNullOrWhiteSpace(postParams.OrderBy) && (postParams.OrderBy == "Newest" || postParams.OrderBy == "Oldest"))
        {
            query = postParams.OrderBy switch
            {
                "Newest" => query.OrderByDescending(p => p.DateCreated),
                _ => query.OrderBy(p => p.DateCreated)
            };
        }

        if (!string.IsNullOrWhiteSpace(postParams.Username))
        {
            var postUser = await _context.Users.AsNoTracking().Where(u => u.UserName == postParams.Username).FirstOrDefaultAsync();

            if (postUser != null)
            {
                // user exists
                query = query.Where(p => p.PostUserId == postUser.Id);
            }
        }

        return await PagedList<PostDTO>.CreateAsync(
                query.ProjectTo<PostDTO>(_mapper.ConfigurationProvider)
                     .AsNoTracking(),
                postParams.PageNumber, postParams.PageSize);
    }

    public async Task<Guid?> Add(AddPostDTO addPostDTO)
    {
        var post = _mapper.Map<Post>(addPostDTO);

        if (post != null)
        {
            post.Id = Guid.NewGuid();
            post.DateCreated = DateTime.UtcNow;
        } else
        {
            return null;
        }

        await _context.Posts.AddAsync(post);

        return post.Id;
    }

    // Add photo
    public async Task<bool> AddPhoto(string postId, IFormFile file)
    {
        if (string.IsNullOrWhiteSpace(postId))
        {
            return false;
        }

        var post = await _context.Posts.AsNoTracking().Include(p => p.Photos)
                            .FirstOrDefaultAsync(p => p.Id.ToString() == postId);

        if (post == null)
        {
            return false;
        }

        var result = await _photoService.AddPhotoAsync(file);

        if (result.Error != null)
        {
            return false;
        }

        var photo = new Photo
        {
            Id = Guid.NewGuid(),
            Url = result.SecureUrl.AbsoluteUri,
            PublicId = result.PublicId,
            PostId = post.Id
        };

        _context.Photos.Add(photo);

        return true;
    }

    // delete photo
    public async Task<bool> DeletePhoto(string photoId)
    {
        if (string.IsNullOrWhiteSpace(photoId))
        {
            return false;
        }

        var photo = await _context.Photos.FindAsync(Guid.Parse(photoId));

        if (photo == null)
        {
            return false;
        }

        if (string.IsNullOrWhiteSpace(photo.PublicId))
        {
            return false;
        }

        var res = await _photoService.DeletePhotoAsync(photo.PublicId);

        if (res.Error != null)
        {
            return false;
        }

        _context.Photos.Remove(photo);

        return true;
    }

    
    // EDIT
    public async Task<bool> Update(PostDTO postDTO)
    {
        if (string.IsNullOrWhiteSpace(postDTO.Title) || string.IsNullOrWhiteSpace(postDTO.Description) ||
            postDTO.Title.Length < 6 || postDTO.Title.Length > 25 || postDTO.Description.Length < 10 || postDTO.Description.Length > 250)
        {
            return false;
        }

        var currentPost = await _context.Posts.FirstAsync(p => p.Id == postDTO.Id);

        if (currentPost != null)
        {
            currentPost.Description = postDTO.Description;
            currentPost.Title = postDTO.Title;
            currentPost.PostLocationId = postDTO.PostLocationId;
            currentPost.PostCategoryId = postDTO.PostCategoryId;

            _context.Posts.Update(currentPost);

            return true;
        }

        return false;
    }

    // delete
    public async Task<bool> Delete(string postId)
    {
        var currentPost = await _context.Posts.FirstAsync(p => p.Id.ToString() == postId);

        if (currentPost != null)
        {
            _context.Posts.Remove(currentPost);

            return true;
        }

        return false;
    }
}
