﻿namespace UDEMY_DOTNET_API_6.Data;
public class LocationsRepository : ILocationsRepository
{
    private readonly DataContext _context;
    private readonly IMapper _mapper;

    public LocationsRepository(DataContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<IEnumerable<LocationDTO>> GetAll()
    {
        return await _context.Locations.AsNoTracking().Select(l => _mapper.Map<LocationDTO>(l)).ToListAsync();
    }
}
