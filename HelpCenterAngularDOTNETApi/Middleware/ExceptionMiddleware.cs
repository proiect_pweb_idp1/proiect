﻿namespace UDEMY_DOTNET_API_6.Middleware;
public class ExceptionMiddleware
{
    private readonly RequestDelegate _next;

    private readonly ILogger<ExceptionMiddleware> _logger;

    private readonly IHostEnvironment _env;

    public ExceptionMiddleware(RequestDelegate next,  ILogger<ExceptionMiddleware> logger, IHostEnvironment env)
    {
        _next = next;
        _logger = logger;
        _env = env;
    }

    // whenever an exception gets thrown in our http middleware, it will be moved upwards in the middleware tree
    // until it reaches the first middleware (this one), and here we try to invoke the next http request in the pipeline
    // and we handle possible exceptions
    public async Task InvokeAsync(HttpContext context, IUnitOfWork unitOfWork)
    {
        try
        {
            await _next(context);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, ex.Message);

            await unitOfWork.LogsRepository.Add(new Log
            {
                Id = Guid.NewGuid(),
                DateCreated = DateTime.Now,
                TableName = "Users",
                Event = ex.Message,
                Remarks = ex.StackTrace?.ToString()
            });

            context.Response.ContentType = "application/json";

            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            var response = _env.IsDevelopment()
                ? new ApiException(context.Response.StatusCode, ex.Message, ex.StackTrace?.ToString())
                : new ApiException(context.Response.StatusCode, "Internal Server Error");

            var options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };

            var json = JsonSerializer.Serialize(response, options);

            await context.Response.WriteAsync(json);
        }
    }
}
