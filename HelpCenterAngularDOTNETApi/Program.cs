var builder = WebApplication.CreateBuilder(args);

// add services to the container
// .net5.0 equivalent: Startup -> ConfigureServices(IServiceCollection services) method

builder.Services.AddApplicationServices(builder.Configuration);

builder.Services.AddControllers();

builder.Services.AddCors();

builder.Services.AddIdentityServices(builder.Configuration);

builder.Services.AddSignalR();

// Configure the HTTP request pipeline
// .net5.0 equivalent: public void Configure(IApplicationBuilder app, IWebHostEnvironment env)

var app = builder.Build();

app.UseMiddleware<ExceptionMiddleware>();

app.UseHttpsRedirection();

app.UseCors(x => x.AllowAnyHeader()
        .AllowAnyMethod()
        .AllowCredentials() // for SignalR
        .WithOrigins("https://localhost:4200"));

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();
app.MapHub<PresenceHub>("hubs/presence");
app.MapHub<MessageHub>("hubs/message");

// .net5.0 equivalent: 
// seeding the data from Program.cs

using var scope = app.Services.CreateScope();

var services = scope.ServiceProvider;

try
{
    var context = services.GetRequiredService<DataContext>();

    await context.Database.MigrateAsync();

    await Seed.SeedAll(context, services.GetRequiredService<IMapper>());
}
catch (Exception ex)
{
    var logger = services.GetService<ILogger<Program>>();

    logger.LogError(ex, "An error occured during migration!");

    var context = services.GetRequiredService<DataContext>();

    var log = new Log { DateCreated = DateTime.Now, Id = Guid.NewGuid(), TableName = "Generic - Migration / Seed Error", Event = ex?.Message, Remarks = ex?.StackTrace };

    context.Logs.Add(log);
}

//try
//{
//    var context = services.GetRequiredService<DataContext>();

//    var userManager = services.GetRequiredService<UserManager<AppUser>>();

//    var roleManager = services.GetRequiredService<RoleManager<AppRole>>();

//    await context.Database.MigrateAsync();

//    await Seed.SeedUsers(userManager, roleManager);
//}
//catch (Exception ex)
//{
//    var logger = services.GetService<ILogger<Program>>();

//    logger.LogError(ex, "An error occured during migration!");
//}

await app.RunAsync();
