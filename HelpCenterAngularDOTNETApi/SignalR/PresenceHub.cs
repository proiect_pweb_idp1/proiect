﻿namespace UDEMY_DOTNET_API_6.SignalR;

[Authorize]
public class PresenceHub : Hub
{
    private readonly PresenceTracker _tracker;

    public PresenceHub(PresenceTracker tracker)
    {
        _tracker = tracker;
    }

    public override async Task OnConnectedAsync()
    {
        // update our presence tracker, we connected
        var isOnline = await _tracker.UserConnected(Context.User.GetUserId(),
            Context.ConnectionId);
        if (isOnline)
        {
            await Clients.Others.SendAsync("UserIsOnline",
               Context.User.GetUserId());
        }

        var currentUsers = await _tracker.GetOnlineUsers();
        // send back the list of all connected users only to the caller;
        // the other users will receive only the current user notification (above statement)
        // if it has only one connection active right now (this one)
        await Clients.Caller.SendAsync("GetOnlineUsers", currentUsers);
    }

    public override async Task OnDisconnectedAsync(Exception ex)
    {
        // update our presence tracker, we disconnected, but we notify only if we dont have any more connections active
        // for this current user
        var isOffline = await _tracker.UserDisconnected(Context.User.GetUserId(),
            Context.ConnectionId);
        if (isOffline)
        {
            await Clients.Others.SendAsync("UserIsOffline",
            Context.User.GetUserId());
        }

        await base.OnDisconnectedAsync(ex);
    }
}
