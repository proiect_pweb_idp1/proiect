﻿namespace UDEMY_DOTNET_API_6.SignalR;
public class PresenceTracker
{
    // key: user's id, value: list of connection IDs
    private static readonly Dictionary<string, List<string>> OnlineUsers
        = new Dictionary<string, List<string>>();

    // concurrent threading here! we need to lock the dictionary!
    public Task<bool> UserConnected(string id, string connectionId)
    {
        bool isOnline = false;

        lock (OnlineUsers)
        {
            if (OnlineUsers.ContainsKey(id))
            {
                OnlineUsers[id].Add(connectionId);
            }
            else
            {
                OnlineUsers.Add(id, new List<string> { connectionId });
                isOnline = true;
            }
        }

        return Task.FromResult(isOnline);
    }

    public Task<bool> UserDisconnected(string id, string connectionId)
    {
        bool isOffline = false;

        lock (OnlineUsers)
        {
            if (!OnlineUsers.ContainsKey(id))
            {
                return Task.FromResult(isOffline);
            }

            OnlineUsers[id].Remove(connectionId);

            if (OnlineUsers[id].Count == 0)
            {
                OnlineUsers.Remove(id);
                isOffline = true;
            }
        }

        return Task.FromResult(isOffline);
    }

    public Task<string[]> GetOnlineUsers()
    {
        string[] onlineUsers;

        lock (OnlineUsers)
        {
            onlineUsers = OnlineUsers.OrderBy(k => k.Key)
                            .Select(k => k.Key).ToArray();
        }

        return Task.FromResult(onlineUsers);
    }

    public Task<List<string>> GetConnectionsForUser(string id)
    {
        List<string> connectionIds;

        lock (OnlineUsers)
        {
            connectionIds = OnlineUsers.GetValueOrDefault(id);
        }

        return Task.FromResult(connectionIds);
    }
}
