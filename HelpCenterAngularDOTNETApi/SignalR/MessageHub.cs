﻿namespace UDEMY_DOTNET_API_6.SignalR;
public class MessageHub : Hub
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly IHubContext<PresenceHub> _presenceHub;
    private readonly PresenceTracker _presenceTracker;

    public MessageHub(IUnitOfWork unitOfWork, IMapper mapper,
        IHubContext<PresenceHub> presenceHub,
        PresenceTracker presenceTracker)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
        _presenceHub = presenceHub;
        _presenceTracker = presenceTracker;
    }

    // we are gonna create a group for each pair of users
    // so we can deliver messages between them!
    public override async Task OnConnectedAsync()
    {
        var httpContext = Context.GetHttpContext();

        // we are gonna pass the other username as a query param
        var otherUser = httpContext.Request.Query["user"].ToString();

        var groupName = GetGroupName(Context.User.GetUsername(),
                                     otherUser);

        await Groups.AddToGroupAsync(Context.ConnectionId, groupName);

        var group = await AddToGroup(groupName);

        // notify the other user in the group that we (current user) connect
        await Clients.Group(groupName).SendAsync("UpdatedGroup", group);

        // gets the message thread between the two users involved
        var messages = await _unitOfWork.MessageRepository
            .GetMessageThread(Context.User.GetUsername(),
                              otherUser);

        if (_unitOfWork.HasChanges())
        {
            await _unitOfWork.Complete();
        }

        // only send the messages to the user that connected
        await Clients.Caller
                     .SendAsync("ReceiveMessageThread",
                        messages);
    }

    public async Task SendMessage(CreateMessageDTO createMessageDTO)
    {
        var username = Context.User.GetUsername();

        if (username == createMessageDTO.RecipientUsername.ToLower())
        {
            throw new HubException("You cannot send messages to yourself!");
        }

        var sender = await _unitOfWork.UserRepository.GetUserByUsernameAsync(username);

        var recipient = await _unitOfWork.UserRepository.GetUserByUsernameAsync(createMessageDTO.RecipientUsername);

        if (recipient == null)
        {
            throw new HubException("User not found!");
        }

        var message = new Message
        {
            Sender = sender,
            Recipient = recipient,
            SenderUsername = sender.UserName,
            RecipientUsername = recipient.UserName,
            Content = createMessageDTO.Content
        };

        var groupName = GetGroupName(sender.UserName, recipient.UserName);

        var group = await _unitOfWork.MessageRepository.GetMessageGroup(groupName);

        if (group.Connections.Any(x => x.Username == recipient.UserName))
        {
            message.DateRead = DateTime.UtcNow;
        }
        else
        {
            var connections = await _presenceTracker.GetConnectionsForUser(recipient.UserName);

            if (connections != null)
            {
                await _presenceHub.Clients.Clients(connections)
                    .SendAsync("NewMessageReceived",
                        new { username = sender.UserName, knownAs = sender.UserName }
                );
            }
        }

        _unitOfWork.MessageRepository.AddMessage(message);

        if (await _unitOfWork.Complete())
        {
            await Clients.Group(groupName).SendAsync("NewMessage",
                _mapper.Map<MessageDTO>(message));
        }
    }

    public override async Task OnDisconnectedAsync(Exception ex)
    {
        var group = await RemoveFromMessageGroup();

        await Clients.Group(group.Name).SendAsync("UpdatedGroup", group);

        await base.OnDisconnectedAsync(ex);
    }

    private async Task<Group> AddToGroup(string groupName)
    {
        var group = await _unitOfWork.MessageRepository.GetMessageGroup(groupName);

        var connection = new Connection(Context.ConnectionId,
            Context.User.GetUsername());

        if (group == null)
        {
            group = new Group(groupName);
            _unitOfWork.MessageRepository.AddGroup(group);
        }

        group.Connections.Add(connection);

        if (await _unitOfWork.Complete())
        {
            return group;
        }

        throw new HubException("Failed to join group.");
    }

    private async Task<Group> RemoveFromMessageGroup()
    {
        var group = await _unitOfWork.MessageRepository.GetGroupForConnection(Context.ConnectionId);

        var connection = group.Connections.FirstOrDefault(c => c.ConnectionId == Context.ConnectionId);

        _unitOfWork.MessageRepository.RemoveConnection(connection);

        if (await _unitOfWork.Complete())
        {
            return group;
        }

        throw new HubException("Failed to remove from group.");
    }

    // returns the group name in alphabetical order
    // (ex: lisa-todd, bob-lisa)
    private string GetGroupName(string caller, string other)
    {
        var stringCompare = string.CompareOrdinal(caller, other) < 0;

        return stringCompare ? $"{caller}-{other}" : $"{other}-{caller}";
    }
}
