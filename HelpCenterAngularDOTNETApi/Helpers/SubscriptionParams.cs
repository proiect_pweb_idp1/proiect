﻿namespace UDEMY_DOTNET_API_6.Helpers;
public class SubscriptionParams : PaginationParams
{
    public string LocationId { get; set; } // the post's location Id, if not null
    public string CategoryId { get; set; } // the post's category Id, if not null
    public string ForUsername { get; set; } // the username the subscription is for
    public string ByUsername { get; set; } // the username the subscription is made by
    public string OrderBy { get; set; } = "Newest"; // "Newest" or "Oldest"
}
