﻿namespace UDEMY_DOTNET_API_6.Helpers;
public class MessageParams : PaginationParams
{
    public string Username { get; set; } // the logged in user's messages
    public string Container { get; set; } = "Unread"; // what messages? "Unread" / "Inbox" / "Outbox" etc
}
