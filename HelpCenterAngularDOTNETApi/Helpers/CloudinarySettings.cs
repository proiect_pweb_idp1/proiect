﻿namespace UDEMY_DOTNET_API_6.Helpers;
public class CloudinarySettings
{
    public string CloudName { get; set; }
    public string ApiKey { get; set; }
    public string ApiSecret { get; set; }
}
