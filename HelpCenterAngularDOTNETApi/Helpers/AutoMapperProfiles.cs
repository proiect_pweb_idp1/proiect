﻿namespace UDEMY_DOTNET_API_6.Helpers;
public class AutoMapperProfiles : Profile
{
    public AutoMapperProfiles()
    {
        CreateMap<AppUser, MemberDTO>()
            .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.UserName));
        CreateMap<Photo, PhotoDTO>();
        CreateMap<PhotoDTO, Photo>();
        CreateMap<Location, LocationDTO>();
        CreateMap<Category, CategoryDTO>();
        CreateMap<RegisterDTO, AppUser>()
            .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Email))
            .ForMember(dest => dest.DateCreated, opt => opt.MapFrom(src => DateTime.UtcNow))
            .ForMember(dest => dest.LastActive, opt => opt.MapFrom(src => DateTime.UtcNow));
        CreateMap<Language, LanguageDTO>();
        CreateMap<TranslationDTO, Translation>();
        CreateMap<Translation, TranslationDTO>()
            .ForMember(dest => dest.LanguageName, opt => opt.MapFrom(src => src.Language.Name));
        CreateMap<Report, ReportDTO>()
            .ForMember(dest => dest.ReportReasonName, opt => opt.MapFrom(src => src.ReportReason.Name));
        CreateMap<Subscription, SubscriptionDTO>()
            .ForMember(dest => dest.ForLocationName,
                               opt => opt.MapFrom((src, dest) => src.ForLocation != null ? src.ForLocation.Name : null))
            .ForMember(dest => dest.ForCategoryName, opt => opt.MapFrom((src, dest) => src.ForCategory != null ? src.ForCategory.Name : null))
            .ForMember(dest => dest.ForUsername, opt => opt.MapFrom((src, dest) => src.ForUser != null ? src.ForUser.UserName : null))
            .ForMember(dest => dest.ByUsername, opt => opt.MapFrom((src, dest) => src.ByUser != null ? src.ByUser.UserName : null));
        CreateMap<MemberUpdateDTO, AppUser>();
        CreateMap<AddSubscriptionDTO, Subscription>()
            .ForMember(dest => dest.ForUserId, opt => opt.MapFrom((src, dest) => !string.IsNullOrWhiteSpace(src.ForUserId) ? src.ForUserId : null))
            .ForMember(dest => dest.ForLocationId,
                    opt => opt.MapFrom((src, dest) => !string.IsNullOrWhiteSpace(src.ForLocationId) ? Guid.Parse(src.ForLocationId) : Guid.Empty))
            .ForMember(dest => dest.ForCategoryId,
                    opt => opt.MapFrom((src, dest) => !string.IsNullOrWhiteSpace(src.ForCategoryId) ? Guid.Parse(src.ForCategoryId) : Guid.Empty))
            .ForMember(dest => dest.DateCreated, opt => opt.MapFrom(src => DateTime.UtcNow));
        CreateMap<MemberDTO, AppUser>();
        CreateMap<Message, MessageDTO>();
        CreateMap<Post, PostDTO>()
            .ForMember(dest => dest.PostLocationName, opt => opt.MapFrom((src, dest) => src.PostLocation != null ? src.PostLocation.Name : null))
            .ForMember(dest => dest.PostCategoryName, opt => opt.MapFrom((src, dest) => src.PostCategory != null ? src.PostCategory.Name : null))
            .ForMember(dest => dest.PostUsername, opt => opt.MapFrom((src, dest) => src.PostUser != null ? src.PostUser.UserName : null));
        CreateMap<PostDTO, Post>();
        CreateMap<AddPostDTO, Post>();
    }
}
