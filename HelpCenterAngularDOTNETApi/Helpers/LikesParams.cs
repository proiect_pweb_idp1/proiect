﻿namespace UDEMY_DOTNET_API_6.Helpers;
public class LikesParams : PaginationParams
{
    public int UserId { get; set; }
    public string Predicate { get; set; }
}
