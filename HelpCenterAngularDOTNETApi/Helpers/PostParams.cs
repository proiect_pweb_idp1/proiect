﻿namespace UDEMY_DOTNET_API_6.Helpers;
public class PostParams : PaginationParams
{
    public string LocationId { get; set; } // the post's location Id, if not null
    public string CategoryId { get; set; } // the post's category Id, if not null
    public string Username { get; set; } // the username who posted the post
    public string OrderBy { get; set; } = "Newest"; // "Newest" or "Oldest"
}
