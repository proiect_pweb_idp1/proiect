﻿namespace UDEMY_DOTNET_API_6.Extensions;
public static class DateTimeExtensions
{
    public static int CalculateAge(this DateTime dob)
    {
        var today = DateTime.Today;
        var age = today.Year - dob.Year;

        if (dob.Date > today.AddYears(-age))
        {
            age--;
        }

        return age;
    }
}
