﻿namespace UDEMY_DOTNET_API_6.Extensions;
public static class IdentityServiceExtensions
{
    public static IServiceCollection AddIdentityServices(this IServiceCollection services, IConfiguration config)
    {
        string domain = $"https://{config["Auth0:Domain"]}/";

        services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                //options.TokenValidationParameters = new TokenValidationParameters
                //{
                //    ValidateIssuerSigningKey = true,
                //    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["TokenKey"])),
                //    ValidateIssuer = false,
                //    ValidateAudience = false
                //};

                options.Authority = domain;
                options.Audience = config["Auth0:Audience"];

                options.TokenValidationParameters = new TokenValidationParameters
                {
                    NameClaimType = ClaimTypes.NameIdentifier
                };

                options.Events = new JwtBearerEvents
                {
                    OnMessageReceived = context =>
                    {
                        // SignalR sends this token
                        var accessToken = context.Request
                                        .Query["access_token"];

                        // verify the path, it needs to come from
                        // one of our hubs
                        var path = context.HttpContext.Request.Path;

                        if (!string.IsNullOrWhiteSpace(accessToken) &&
                            path.StartsWithSegments("/hubs")) // needs to start with "/"
                        {
                            context.Token = accessToken;
                        }

                        return Task.CompletedTask;
                    }
                };
            });

        services.AddAuthorization(opt =>
        {
            opt.AddPolicy("help-center:volunteer", policy =>
            {
                policy.Requirements.Add(new HasScopeRequirement("help-center:volunteer", domain));
            });
            opt.AddPolicy("help-center:admin", policy =>
            {
                policy.Requirements.Add(new HasScopeRequirement("help-center:volunteer", domain));
                policy.Requirements.Add(new HasScopeRequirement("help-center:admin", domain));
            });
        });

        services.AddSingleton<IAuthorizationHandler, HasScopeHandler>();

        return services;
    }
}
