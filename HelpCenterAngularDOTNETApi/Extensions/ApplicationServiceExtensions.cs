﻿namespace UDEMY_DOTNET_API_6.Extensions;
public static class ApplicationServiceExtensions
{
    public static IServiceCollection AddApplicationServices(this IServiceCollection services, IConfiguration config)
    {
        services.AddSingleton<PresenceTracker>();

        services.Configure<CloudinarySettings>(config.GetSection("CloudinarySettings"));

        services.AddScoped<ITokenService, TokenService>();

        services.AddScoped<IManagementAPIService, ManagementAPIService>();

        services.AddScoped<IPhotoService, PhotoService>();

        services.AddScoped<IUnitOfWork, UnitOfWork>();

        services.AddScoped<LogUserActivity>();

        services.AddAutoMapper(typeof(AutoMapperProfiles).Assembly);

        services.AddDbContext<DataContext>(options =>
        {
            options.UseSqlServer(config.GetConnectionString("HelpCenterDbString"));
        });

        return services;
    }
}
