﻿namespace UDEMY_DOTNET_API_6.Extensions;
public static class HttpExtensions
{
    public static void AddPaginationHeader(this HttpResponse response, int currentPage,
                                           int itemsPerPage, int totalItems, int totalPages)
    {
        var paginationHeader = new PaginationHeader(currentPage, itemsPerPage, totalItems, totalPages);

        var opt = new JsonSerializerOptions
        {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
        };

        response.Headers.Add("Pagination", JsonSerializer.Serialize(paginationHeader, opt));

        response.Headers.Add("Access-Control-Expose-Headers", "Pagination");
    }
}
